﻿
namespace ApiControlMiner.Models
{
    public class StartParams
    {
        public string LoginUser { get; set; }
        public string Token { get; set; }
        public string WorkingDirectory { get; set; }
        public bool IsProduction { get; set; }
    }
}
