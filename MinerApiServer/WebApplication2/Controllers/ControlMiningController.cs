﻿using System;
using System.Threading.Tasks;
using ApiControlMiner.Models;
using AudioMiner.BLL.Implementation.Services.Mining;
using Microsoft.AspNetCore.Mvc;
using System.Threading;

namespace ApiControlMiner.Controllers
{
    [Route("api/control-mining/")]
    public class ControlMiningController : Controller
    {
        private static MiningService mining = MiningService.getInstance();
        private static DateTime LastTimePing = DateTime.Now;
        private static Timer TimerCheckPing = new Timer(CheckPing, null, 0, 20 * 1000); // every 20 second

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginUser">A unique login for the user to access his account in the pool</param>
        /// <param name="token">User authorization token to access the site server</param>
        /// <returns></returns>
        // POST api/control-mining/start
        [HttpPost("start")]
        public async Task<IActionResult> StartAsync([FromBody] StartParams data)
        {
            Console.WriteLine("api start");

            if (mining.IsMining)
            {
                return Ok(true);
            }

            if (data == null)
            {
                return BadRequest("Empty params");
            }

            Console.WriteLine("loginUser='" + data.LoginUser + "'");
            Console.WriteLine("token='" + data.Token + "'");
            Console.WriteLine("WorkingDirectory='" + data.WorkingDirectory + "'");

            if (data.LoginUser == null || data.LoginUser == "")
            {
                return BadRequest("Empty loginUser");
            }

            if (data.Token == null || data.Token == "")
            {
                return BadRequest("Empty token");
            }

            if (data.WorkingDirectory == null || data.WorkingDirectory == "")
            {
                return BadRequest("Empty workingDirectory");
            }

            mining.errors.Clear();
            mining.dataParams = data;

            if (!await mining.Launch())
            {
                return BadRequest(mining.errors.ToArray());
            }

            Console.WriteLine("mining.IsMining=" + mining.IsMining);
            return Ok(true);
        }

        // POST api/control-mining/stop
        [HttpPost("stop")]
        public IActionResult Stop()
        {
            Console.WriteLine("api stop");

            if (!mining.IsMining)
            {
                return Ok(true);
            }

            if (mining == null)
            {
                return BadRequest(new string[] { "Mining has not yet been created" });
            }

            if (!mining.Stop())
            {
                return BadRequest(mining.errors.ToArray());
            }

            return Ok(true);
        }

        // POST api/control-mining/ping
        [HttpPost("ping")]
        public IActionResult Ping()
        {
            Console.WriteLine("api Ping");

            LastTimePing = DateTime.Now;
            return Ok(mining.IsMining);
        }

        // POST api/control-mining/off-server
        [HttpPost("off-server")]
        public IActionResult Off()
        {
            Console.WriteLine("api Off");

            mining.Stop();
            Environment.Exit(0);
            return Ok(true);
        }

        private static void CheckPing(object o)
        {
            Console.WriteLine("CheckPing");
            if (mining != null && mining.IsMining && (DateTime.Now - LastTimePing).TotalMilliseconds > 15 * 1000)
            {
                Console.WriteLine("CheckPing stop miner");
                mining.Stop();
            }
        }
    }
}
