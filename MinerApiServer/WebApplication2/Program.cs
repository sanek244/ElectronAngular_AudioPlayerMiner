﻿using ApiControlMiner.Controllers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Runtime.InteropServices;

namespace ApiControlMiner
{
    public class Program
    {
        //The comment lines are needed and work only for the Windows platform - they hide the server console
        /*[DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        */

        const int SW_HIDE = 0;

        public static void Main(string[] args)
        {
            //hide Console
            /*var handle = GetConsoleWindow();  
            ShowWindow(handle, SW_HIDE);*/

            BuildWebHost(args).Run();
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("Close application");
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://localhost:7787")
                .Build();
    }
}
