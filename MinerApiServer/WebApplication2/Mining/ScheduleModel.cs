﻿namespace AudioMiner.MVVM.Settings
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public class ScheduleModel : INotifyPropertyChanged
    {
        public static readonly string Friday = "FRI";

        public static readonly string Monday = "MON";

        public static readonly string Saturday = "SAT";

        public static readonly string Sunday = "SUN";

        public static readonly string Thursday = "THU";

        public static readonly string Tuesday = "TUE";

        public static readonly string Wednesday = "WED";

        public ScheduleModel()
        {
            this.Schedule = new Dictionary<string, List<TimeSpan[]>>()
                                {
                                    { Monday, null },
                                    { Tuesday, null },
                                    { Wednesday, null },
                                    { Thursday, null },
                                    { Friday, null },
                                    { Saturday, null },
                                    { Sunday, null }
                                };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Dictionary<string, List<TimeSpan[]>> Schedule { get; set; }

        public List<TimeSpan[]> this[string key]
        {
            get => Schedule[key];

            set
            {
                Schedule[key] = value;
            }
        }
    }
}
