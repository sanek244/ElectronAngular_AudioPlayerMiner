namespace AudioMiner.BLL.Implementation.Services.Mining
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    using AudioMiner.BLL.Abstraction.Models;
    using AudioMiner.SAL.Abstraction;
    using AudioMiner.SAL.Abstraction.Rest.Auth;

    using MultiMiner.Engine;
    using MultiMiner.Engine.Data;
    using MultiMiner.Engine.Installers;
    using MultiMiner.Xgminer;
    using MultiMiner.Xgminer.Api;
    using MultiMiner.Xgminer.Data;
    using MiningPool = MultiMiner.Xgminer.Data.MiningPool;

    using Newtonsoft.Json;
    using Refit;
    using ApiControlMiner.Models;
    using System.Runtime.InteropServices;

    public class MiningService
    {
        private const string MacExecutableName = "bin/bfgminer";

        private string PathMiner {
            get{
                string path = "BFGMiner";
                if(dataParams != null && dataParams.IsProduction == false)
                {
                    path += RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "Win" : "Mac";
                }
                return path; 
            }
        }

        private string UserAgent = "MultiMiner/V3-Example";

        private const string WinExecutableName = "bfgminer.exe";


        private readonly ISettingsApi settingsApi;

        private ApiContext _api;

        private bool _isConfigurated;

        private bool _isPrepared;

        private Miner _miner;

        private MultiMiner.Xgminer.Data.Configuration.Miner _minerConfiguration;

        private Process _minerProcess;

        public StartParams dataParams;

        public List<string> errors;

        private static MiningService instance = null;

        private MiningService()
        {
            this.settingsApi = RestService.For<ISettingsApi>(Constants.API.BaseUrl);
        }

        public static MiningService getInstance()
        {
            if (instance == null)
                instance = new MiningService();

            instance.errors = new List<string>();

            return instance;
        }


        public bool IsMining {
            get {
                var processes = Process.GetProcessesByName("bfgminer");
                return processes.Length > 0;
            }
        }

        public async Task<bool> Launch()
        {
            Console.WriteLine("Launch");
            bool isOk = true;

            if (!_isPrepared)
            {
                PrepareMiner();
            }

            if (!_isConfigurated)
            {
                await ConfigurateMiner();
            }

            try
            {
                Console.WriteLine("_miner.Launch");
                _minerProcess = _miner.Launch();
                Console.WriteLine("IsMining = true");
            }
            catch (Exception error)
            {
                Console.WriteLine("error= " + error.Message);
                errors.Add(error.Message);
                isOk = false;
            }

            return isOk;
        }

        public Task PrepareMiner()
        {
            Console.WriteLine("PrepareMiner");
            return Task.Factory.StartNew(() =>
            {
                try
                {
                    var availableMiners = AvailableMiners.GetAvailableMiners(UserAgent);
                    var bfgminer = availableMiners.Single(am => am.Name.Equals(MinerNames.BFGMiner, StringComparison.OrdinalIgnoreCase));
                    MinerInstaller.InstallMiner(UserAgent, bfgminer, Path.Combine(dataParams.WorkingDirectory, PathMiner));
                    _isPrepared = true;
                    Console.WriteLine("_isPrepared=true");
                }
                catch (Exception error)
                {
                    Console.WriteLine("error= " + error.Message);
                    errors.Add(error.Message);
                }
            });
        }

        public bool Stop()
        {
            Console.WriteLine("Stop");
            bool isOk = true;

            if (this._api != null)
            {
                try
                {

                    this._api.QuitMining();
                    Console.WriteLine(" QuitMining = true");
                }
                catch (Exception error)
                {
                    Console.WriteLine("error= " + error.Message);
                    errors.Add(error.Message);
                    isOk = false;
                }
            }

            if (this._minerProcess != null)
            {
                try
                {
                    this._minerProcess.Kill();
                    this._minerProcess.WaitForExit();
                    this._minerProcess.Close();
                    Console.WriteLine(" this.IsMining = false");
                }
                catch (Exception error)
                {
                    Console.WriteLine("error= " + error.Message);
                    errors.Add(error.Message);
                    isOk = false;
                }
            }

            if(_api == null && _minerProcess == null)
            {
                var processes = Process.GetProcessesByName("bfgminer");
                foreach(var process in processes)
                {
                    process.Kill();
                }
            }

            return isOk;
        }

        private async Task ConfigurateMiner()
        {
            var executablePath = Environment.OSVersion.VersionString.IndexOf("Windows") != -1
                                     ? WinExecutableName
                                     : MacExecutableName;
            try
            {
                Console.WriteLine("ConfigurateMiner");
                this._minerConfiguration =
                    new MultiMiner.Xgminer.Data.Configuration.Miner
                    {
                        ExecutablePath = Path.Combine(dataParams.WorkingDirectory, PathMiner, executablePath),
                        DisableUsbProbe = true,
                    };
                Console.WriteLine("ExecutablePath="+ Path.Combine(dataParams.WorkingDirectory, PathMiner, executablePath));
                this._miner = new MultiMiner.Xgminer.Miner(this._minerConfiguration, false);

                //get config before ListDevices, because it starts mining. If token is expired, user will redirect to login page
                Console.WriteLine("GetXmlConfig");
                var config = await this.GetXmlConfig();
                Console.WriteLine("GetXmlConfig=true");

                var devices = _miner.ListDevices();
                if (devices.Count == 0)
                {
                    Console.WriteLine("error=No devices capable of mining detected");
                    throw new Exception("No devices capable of mining detected");
                }
                Console.WriteLine("devices.Count=" + devices.Count);
                this._minerConfiguration.DeviceDescriptors.AddRange(devices);
                this._minerConfiguration.Algorithm = MinerFactory.Instance.GetAlgorithm(AlgorithmNames.SHA256);
                this._minerConfiguration.ApiListen = true;
                this._minerConfiguration.ApiPort = 4028;
                var pool = config.ArrayOfCoinConfiguration.CoinConfiguration.Pools.MiningPools;
                this._minerConfiguration.Pools.Add(
                    new MiningPool()
                    {
                        Host = pool.Host.Split('/')[2],
                        Password = pool.Password,
                        Username = pool.Username,
                        Port = pool.Port,
                    //Quota =pool.Quota,
                    //QuotaEnabled = pool.QuotaEnabled
                });
                _minerConfiguration.CoinName = config.ArrayOfCoinConfiguration.CoinConfiguration.Coins.Name;
                this._miner = new MultiMiner.Xgminer.Miner(this._minerConfiguration, false);
                this._api = new MultiMiner.Xgminer.Api.ApiContext(this._minerConfiguration.ApiPort);
                this._isConfigurated = true;
                Console.WriteLine("_isConfigurated=true");
            }
            catch(Exception error)
            {
                Console.WriteLine("error= " + error.Message + " workFile=" + Path.Combine(dataParams.WorkingDirectory, PathMiner, executablePath));
                errors.Add(error.Message);
            }
        }

        private async Task<MiningConfig> GetXmlConfig()
        {
            Console.WriteLine("GetXmlConfig token=" + dataParams.Token);
            var xml = await settingsApi.GetXmlConfig("Bearer " + dataParams.Token);
            var _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (xml.StartsWith(_byteOrderMarkUtf8))
            {
                xml = xml.Remove(0, _byteOrderMarkUtf8.Length);
            }

            xml = xml.Replace("\\r\\n", "").Replace("\\", "");
            if (xml.EndsWith("\""))
            {
                xml = xml.Remove(xml.Length - 1, 1);
            }
            var doc = new XmlDocument();
            try
            {
                doc.LoadXml(xml);
            }
            catch (Exception error)
            {
                Console.WriteLine("error= " + error.Message);
                errors.Add(error.Message);
            }

            var json = JsonConvert.SerializeXmlNode(doc);
            var result = JsonConvert.DeserializeObject<MiningConfig>(json);
            return result;
        }

        private async Task<IEnumerable<MiningPool>> GetMiningPools()
        {
            Console.WriteLine("GetMiningPools loginUser=" + dataParams.LoginUser);
            var pools = new List<MiningPool>
                            {
                                new MiningPool
                                    {
                                        Host = "eu.multipool.us",
                                        Port = 3332,
                                        Username = $"ivan_marinin.{dataParams.LoginUser}",
                                        Password = "123"
                                    }
                            };
            return pools;
        }
    }
}
