﻿namespace AudioMiner.BLL.Abstraction.Models
{
    using Newtonsoft.Json;

    public class MiningConfig
    {
        [JsonProperty(PropertyName = "?xml")]
        public XmlData XmlData { get; set; }

        [JsonProperty(PropertyName = "ArrayOfCoinConfiguration")]
        public ArrayOfCoinConfiguration ArrayOfCoinConfiguration { get; set; }
    }

    public class ArrayOfCoinConfiguration
    {
        [JsonProperty(PropertyName = "CoinConfiguration")]
        public CoinConfiguration CoinConfiguration { get; set; }
    }

    public class XmlData
    {
        [JsonProperty(PropertyName = "@version")]
        public string Version { get; set; }
        [JsonProperty(PropertyName = "@encoding")]
        public string Encoding { get; set; }
    }

    public class CoinConfiguration
    {
        [JsonProperty(PropertyName = "Coin")]
        public Coin Coins { get; set; }

        [JsonProperty(PropertyName = "Pools")]
        public Pool Pools { get; set; }
    }

    public class Pool
    {
        [JsonProperty(PropertyName = "MiningPool")]
        public MiningPool MiningPools { get; set; }
    }

    public class MiningPool
    {
        [JsonProperty(PropertyName = "Host")]
        public string Host { get; set; }

        [JsonProperty(PropertyName = "Port")]
        public int Port { get; set; }

        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "Password")]
        public string Password { get; set; }

		[JsonProperty(PropertyName = "Quota")]
		public int Quota { get; set; }

		[JsonProperty(PropertyName = "QuotaEnabled")]
		public bool QuotaEnabled { get; set; }
    }

    public class Coin
    {
        [JsonProperty(PropertyName = "Symbol")]
        public string Symbol { get; set; }

        [JsonProperty(PropertyName = "Kind")]
        public string Kind { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Algorithm")]
        public string Algorithm { get; set; }
    }

}
