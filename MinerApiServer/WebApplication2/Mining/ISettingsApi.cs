﻿namespace AudioMiner.SAL.Abstraction.Rest.Auth
{
    using System.Threading.Tasks;

    using Refit;


    public interface ISettingsApi
    {
        [Get("/api/Settings/xmlconfig")]
        Task<string> GetXmlConfig([Header("Authorization")] string token);
    }
}
