"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DragDropListDirective = (function () {
    function DragDropListDirective(el) {
        this.el = el;
        this.draggable = true;
        this.onDrop = new core_1.EventEmitter();
        this.shiftDown = false;
    }
    DragDropListDirective.prototype.dragstart = function ($event) {
        var dragIndex = this.dragDropList[0].toString();
        $event.dataTransfer.setData('dragIndex', dragIndex);
        this.addDraggedStyle();
    };
    DragDropListDirective.prototype.dragend = function () {
        this.removeDraggedStyle();
    };
    DragDropListDirective.prototype.dragover = function ($event) {
        $event.preventDefault();
        this.addDragOveredStyle();
    };
    DragDropListDirective.prototype.dragleave = function () {
        this.removeDragOveredStyle();
    };
    DragDropListDirective.prototype.drop = function ($event) {
        var dropIndex = this.dragDropList[0];
        var dragIndex = +$event.dataTransfer.getData('dragIndex');
        var array = this.dragDropList[1];
        this.removeDragOveredStyle();
        if (dropIndex !== dragIndex) {
          const temp = array[dragIndex];
          const arr1 = array.slice(0, dragIndex).concat(array.slice(dragIndex + 1));
          array = arr1.slice(0, dropIndex).concat(temp, arr1.slice(dropIndex));
          this.onDrop.emit(array);
        }
    };
    DragDropListDirective.prototype.removeDragOveredStyle = function () {
        if (this.draggOvered) {
            this.el.nativeElement.classList.remove(this.draggOvered);
        }
        else {
            this.el.nativeElement.style.outline = 'none';
        }
    };
    DragDropListDirective.prototype.addDragOveredStyle = function () {
        if (this.draggOvered) {
            this.el.nativeElement.classList.add(this.draggOvered);
        }
        else {
            this.el.nativeElement.style.outline = '2px dashed lightblue';
        }
    };
    DragDropListDirective.prototype.addDraggedStyle = function () {
        if (this.dragged) {
            this.el.nativeElement.classList.add(this.dragged);
        }
        else {
            this.el.nativeElement.style.opacity = 0.6;
        }
    };
    DragDropListDirective.prototype.removeDraggedStyle = function () {
        if (this.dragged) {
            this.el.nativeElement.classList.remove(this.dragged);
        }
        else {
            this.el.nativeElement.style.opacity = 1;
        }
    };
    DragDropListDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[dragDropList]'
                },] },
    ];
    DragDropListDirective.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
    ]; };
    DragDropListDirective.propDecorators = {
        "dragDropList": [{ type: core_1.Input },],
        "dragged": [{ type: core_1.Input },],
        "draggOvered": [{ type: core_1.Input },],
        "draggable": [{ type: core_1.HostBinding },],
        "onDrop": [{ type: core_1.Output },],
        "dragstart": [{ type: core_1.HostListener, args: ['dragstart', ['$event'],] },],
        "dragend": [{ type: core_1.HostListener, args: ['dragend',] },],
        "dragover": [{ type: core_1.HostListener, args: ['dragover', ['$event'],] },],
        "dragleave": [{ type: core_1.HostListener, args: ['dragleave',] },],
        "drop": [{ type: core_1.HostListener, args: ['drop', ['$event'],] },],
        "keyup": [{ type: core_1.HostListener, args: ['keyup', ['$event'],] },],
        "keydown": [{ type: core_1.HostListener, args: ['keydown', ['$event'],] },],
    };
    return DragDropListDirective;
}());
exports.DragDropListDirective = DragDropListDirective;
//# sourceMappingURL=drag-drop-list.directive.js.map
