import { ElementRef, EventEmitter } from '@angular/core';
export declare class DragDropListDirective {
    private el;
    dragDropList: [number, Array<any>];
    dragged: string;
    draggOvered: string;
    draggable: boolean;
    onDrop: EventEmitter<{}>;
    shiftDown: boolean;
    dragstart($event: DragEvent): void;
    dragend(): void;
    dragover($event: any): void;
    dragleave(): void;
    drop($event: DragEvent): void;
    keyup($event: any): void;
    keydown($event: any): void;
    constructor(el: ElementRef);
    removeDragOveredStyle(): void;
    addDragOveredStyle(): void;
    addDraggedStyle(): void;
    removeDraggedStyle(): void;
    keydownDown(): void;
    keydownUp(): void;
    switchNext(): void;
    switchPrev(): void;
    focusNext(): void;
    focusPrev(): void;
}
