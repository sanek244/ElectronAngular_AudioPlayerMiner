"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drag_drop_list_directive_1 = require("./drag-drop-list.directive");
var common_1 = require("@angular/common");
var DragDropListModule = (function () {
    function DragDropListModule() {
    }
    DragDropListModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [drag_drop_list_directive_1.DragDropListDirective],
                    exports: [drag_drop_list_directive_1.DragDropListDirective]
                },] },
    ];
    DragDropListModule.ctorParameters = function () { return []; };
    return DragDropListModule;
}());
exports.DragDropListModule = DragDropListModule;
//# sourceMappingURL=drag-drop-list.module.js.map