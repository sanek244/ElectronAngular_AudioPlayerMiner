export class Logger{
  public static log(...args: any[]){
    console.log('[log]:', args);
  }

  public static err(...args: any[]){
    console.log('[error]:', args);
  }
}
