import { PlayerComponent } from './pages/player/player.component';
import { AuthorizationComponent } from './pages/authorization/authorization.component';
import {Injectable, NgModule} from '@angular/core';
import {Routes, RouterModule, CanLoad, Route} from '@angular/router';
import {UserService} from './providers/user.service';
import {Observable} from 'rxjs/index';
import {ViewAlbumMusicComponent} from './components/view-album-music/view-album-music.component';
import {ViewAlbumsComponent} from './components/view-albums/view-albums.component';
import {ViewArtistsComponent} from './components/view-artists/view-artists.component';
import {ViewGenresComponent} from './components/view-genres/view-genres.component';
import {ViewMusicComponent} from './components/view-music/view-music.component';
import {ViewPlaylistMusicComponent} from './components/view-playlist-music/view-playlist-music.component';
import { ViewSettingComponent } from './components/view-setting/view-setting.component';


@Injectable()
export class CanLoadPlayer implements CanLoad {
  constructor(private userService: UserService) {}

  canLoad(route: Route): Observable<boolean>|Promise<boolean>|boolean {
    return this.userService.getUser() !== null;
  }
}
const routes: Routes = [
  {
    path: 'player',
    canLoad: [CanLoadPlayer],
    component: PlayerComponent,
    children: [
      {
        path: '',
        component: ViewGenresComponent,
        outlet: 'content'
      },
      {
        path: 'genres/:genre/artists/:idArtist/albums/:idAlbum',
        component: ViewAlbumMusicComponent,
        outlet: 'content'
      },
      {
        path: 'genres/:genre/artists/:idArtist',
        component: ViewAlbumsComponent,
        outlet: 'content'
      },
      {
        path: 'genres/:genre',
        component: ViewArtistsComponent,
        outlet: 'content'
      },
      {
        path: 'artists-all',
        component: ViewArtistsComponent,
        data: { isAllPurchased: true },
        outlet: 'content'
      },
      {
        path: 'albums-all',
        component: ViewAlbumsComponent,
        data: { isAllPurchased: true },
        outlet: 'content'
      },
      {
        path: 'playlist-music/:idPlaylist',
        component: ViewPlaylistMusicComponent,
        outlet: 'content'
      },
      {
        path: 'music-all-aws',
        component: ViewMusicComponent,
        outlet: 'content'
      },
      {
        path: 'music-all-locale',
        component: ViewMusicComponent,
        data: { isLocale: true },
        outlet: 'content'
      },
      {
        path: 'settings',
        component: ViewSettingComponent,
        outlet: 'content'
      },
      {
        path: '**',
        component: ViewGenresComponent,
        outlet: 'content'
      },
    ]
  },
  {
    path: '',
    redirectTo: '/player',
    pathMatch: 'full'
  },
  {
    path: 'authorization',
    component: AuthorizationComponent
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true})],
    providers: [CanLoadPlayer, UserService],
    exports: [RouterModule]
})
export class AppRoutingModule { }
