﻿import {Injectable } from '@angular/core';
import {AuthorizationService} from './authorization.service';
import {HttpParams} from '@angular/common/http';
import {AppConfig} from '../../environments/environment';
import {User} from '../models/User';
import {authorizationServiceType} from './user.service';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class AuthorizationLocaleService extends AuthorizationService{

  private _username;
  private _password;

  private _isSignIn = new BehaviorSubject<boolean>(true); //false -> is registration
  private _isOpenModalForgotPassword = new BehaviorSubject<boolean>(false);

  public isSignIn$ = this._isSignIn.asObservable();
  public isOpenModalForgotPassword$ = this._isOpenModalForgotPassword.asObservable();

  init(){
    this.closeModalForgotPassword = this.closeModalForgotPassword.bind(this);
  }

  public login(username: string, password: string) : Promise<any> {
    if(!username){
      return new Promise((ok,err) => err('Email address must not be empty!'));
    }

    if(!password){
      return new Promise((ok,err) => err('Password must not be empty!'));
    }

    this._username = username;
    this._password = password;
    return this._loginLogic();
  }

  public registration(data): Promise<any> {
    return this.http.post(AppConfig.siteUrl + '/api/Account/registration', data)
      .toPromise()
      .catch(this._processErrors);
  }

  public resetPassword(email): Promise<any> {
    if(!email){
      return new Promise((ok,err) => err('Email address must not be empty!'));
    }

    return this.http.post(AppConfig.siteUrl + '/api/Account/forgot', {email: email})
      .toPromise()
      .then(this.closeModalForgotPassword)
      .catch(this._processErrors);
  }

  set isSignIn(value){
    this._isSignIn.next(value);
  }

  public openModalForgotPassword(){
    this._isOpenModalForgotPassword.next(true);
  }

  public closeModalForgotPassword(){
    this._isOpenModalForgotPassword.next(false);
  }


  protected _onSuccessAuthOrRefreshToken(httpParams: HttpParams, isAuthorization): Promise<string> {
    if(!httpParams){
      return;
    }

    return this._getTokenSite(httpParams)
      .then((token: any) => {
        const user = this.userService.getUser() || new User();
        user.token = token.access_token;
        this.userService.setUser(user);

        if (!user.workerId) {
          return this.userService.getDataFromServer(user.token)
            .then((dataUser: any) => {
              user.workerId = dataUser.workerId;
              user.dateOfBirth = dataUser.dateOfBirth;

              if(isAuthorization){
                const user = new User();
                user.givenName = this._username;
                user.fullName = this._username;
                user.email = this._username;
                user.password = this._password;
                user.serviceAuthorizationName = authorizationServiceType.locale;
                this.userService.setUser(user);

                this.router.navigate(['/']);
                return;
              }

              this.userService.setUser(user);

              return token.access_token;
            });
        }

        return token.access_token;
      })
      .catch(err => {
        if(err.error.error === 'invalid_grant'){
          this.userService.setUser(null);
          if(location.hash === '#/'){
            this.router.navigate(['/authorization']);
            this.toastr.error('Invalid login rights, please log in again.',
              'Error authorization',
              {positionClass: 'toast-bottom-right', timeOut: 15000});
            return;
          }
        }

        throw err;
      });
  }

  protected _getParamsForSite() : HttpParams{
    const user = this.userService.getUser() || new User();

    return new HttpParams()
      .set('username', user.email || this._username)
      .set('password', user.password || this._password)
      .set('grant_type', 'password')
      .set('scope', 'openid email profile offline_access roles');
  }
}



