﻿﻿import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {Music} from '../models/Music';
import {TypeMusicSourceExternal} from '../enums/TypeMusicSource';
import {Artist} from '../models/Artist';
import {AppConfig} from '../../environments/environment';
import {Helper} from '../Helper';
import {Album} from '../models/Album';
import {Logger} from '../Logger';


const AWS = require('aws-sdk');

//https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Credentials.html#accessKeyId-property
const credentials = {
  credentials: {
    accessKeyId: AppConfig.AWS_accessKey,
    secretAccessKey: AppConfig.AWS_secretKey,
  }
};

const S3 = new AWS.S3(credentials);

@Injectable()
export class MusicAWSService {

  public isChangeObserable = new Observable(observer => {
    this.loadStructureS3()
      .then(this.fullFieldsFromStructureS3.bind(this))
      .then(() => observer.next(true));
  });

  public Genres: string[] = [];
  public Artists : Artist[] = [];
  public Albums : Album[] = [];

  private _music : Music[] = [];
  public get Music() : Music[] {
    return [].concat(this._music);
  }

  constructor() {
    this.loadSDK();
  }

  private loadSDK() {
    (function(d, s, id) {
      if (d.getElementById(id)) {
        return;
      }

      const js: any = d.createElement(s); js.id = id;
      js.src = 'assets/aws-sdk.js';

      const fjs = d.getElementsByTagName(s)[0];
      fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'aws-sdk'));
  }

  private loadStructureS3() : Promise<any[]> {
    const key = 'S3.listObjects';
    const data = sessionStorage.getItem(key);
    if(data){
      return new Promise(ok => ok(JSON.parse(data)));
    }

    return this.getStructureS3().then(data => {
      sessionStorage.setItem(key, JSON.stringify(data));
      return data;
    });
  }

  private fullFieldsFromStructureS3(data: any[]){
    this.Genres = JSON.parse(sessionStorage.getItem('genres') || '[]');
    this.Albums = JSON.parse(sessionStorage.getItem('albums') || '[]').map(el => new Album().parse(el));
    this.Artists = JSON.parse(sessionStorage.getItem('artists') || '[]').map(el => new Artist().parse(el));
    this._music = JSON.parse(sessionStorage.getItem('music') || '[]').map(el => new Music().parse(el));

    if(this.Genres.length){
      return;
    }

    data.map(el => {
      const parts = el.Key.split('/');
      const genre = parts.length > 0 ? parts[0] : null;
      const artist = parts.length > 1 ? parts[1] : null;
      const album = parts.length > 2 ? parts[2] : null;
      const music = parts.length > 3 ? parts[3] : null;

      const artistFounded = artist ? this.Artists.find(x => x.name === artist) : null;
      if(music){
        const albumFounded = this.Albums.find(x => x.title === album);
        if(music.substr(-4) === '.jpg' || music.substr(-5) === '.jpeg' || music.substr(-4) === '.png'){
          albumFounded.picture = el.Key;
        }
        else{
          const newMusic = new Music(el.Key, music, TypeMusicSourceExternal.AWS);
          newMusic.idAlbum = albumFounded.id;
          newMusic.idArtist = artistFounded.id;
          newMusic.metaData.artist = artistFounded.name;
          newMusic.metaData.album = albumFounded.title;
          newMusic.metaData.genres = artistFounded.genres;
          this._music.push(newMusic);
        }
      }
      else if(album){
        this.Albums.push(new Album(album, artistFounded.id));
      }
      else if(artist){
        this.Artists.push(new Artist(artist, genre));
      }
      else if(genre) {
        this.Genres.push(genre);
      }
    });

    this.Albums.map(album => {
      if(album.picture){
        this._music.filter(x => x.idAlbum === album.id).map(music => music.metaData.pictureData = album.picture);
      }
    });

    sessionStorage.setItem('genres', JSON.stringify(this.Genres));
    sessionStorage.setItem('albums', JSON.stringify(this.Albums));
    sessionStorage.setItem('artists', JSON.stringify(this.Artists));
    sessionStorage.setItem('music', JSON.stringify(this._music));
  }

  //https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#listObjects-property
  private getStructureS3(marker: string = '') : Promise<any> {
    const params = {
      Bucket: AppConfig.AWS_bucketName,
      MaxKeys: 1000, //1000 it maximum
      Marker: marker //get data begin with el.Key === Marker
    };
    return new Promise((ok, err) => {
      S3.listObjects(params, (error, data) => {
        if (error) {
          Logger.err('S3 listObjects', error);
          err(error);
          return;
        }

        if(data.IsTruncated  === true){
          this.getStructureS3(Helper.last(data.Contents.Key))
            .then(data2 => ok(data.concat(data2.Contents)));
          return;
        }

        ok(data.Contents);
      });
    });
  }

  public getArtistByGenre(genre: string){
    return this.Artists.filter(x => x.genres.indexOf(genre) > -1);
  }

  public getAlbumsByArtistId(artistId: string){
    return this.Albums.filter(x => x.idArtist === artistId);
  }

  public getMusicByAlbumId(albumId: string){
    return this.Music.filter(x => x.idAlbum === albumId);
  }
}
