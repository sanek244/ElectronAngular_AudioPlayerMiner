﻿import {Injectable} from '@angular/core';
import {User} from '../models/User';
import {authorizationServiceType} from './user.service';
import {AuthorizationService } from './authorization.service';
import {AppConfig} from '../../environments/environment';
import {HttpParams} from '@angular/common/http';
import {Logger} from '../Logger';

@Injectable()
export class AuthorizationGoogleService extends AuthorizationService {

  private _token;

  init() {
    this.loadSDK();
  }

  public login() : Promise<any> {
    return super._loginLogic()
      .catch(err => {
        if(err === 'emptyToken') {
          Logger.err('empty token');
          this.router.navigate(['/']);
        }

        throw err;
    });
  }

  protected _login(): Promise<HttpParams> {
    return new Promise((ok, err) => {
      if (!(<any>window).gapi) {
        Logger.log('google download...');
        if(!this.isExistInternet) {
          this.loadSDK();
        }
        setTimeout(() => this.login(), 500);
        return new Promise(ok => ok(null));
      }

      this.auth2Load()
        .then(auth2 => {
          auth2.signIn().then(() => {
            const googleUser = auth2.currentUser.get();
            const profile = googleUser.getBasicProfile();

            const user = new User();
            user.familyName = profile.getFamilyName();
            user.givenName = profile.getGivenName();
            user.fullName = profile.getName();
            user.imageUrl = profile.getImageUrl();
            user.email = profile.getEmail();
            user.tokenSocialNetwork = googleUser.getAuthResponse().access_token;
            user.serviceAuthorizationName = authorizationServiceType.google;
            this.userService.setUser(user);

            if(!user.tokenSocialNetwork) {
              Logger.err('fail google auth', googleUser.getAuthResponse());
            }

            this._token = user.tokenSocialNetwork;
            ok(this._getParamsForSite());
          })
          .catch(err);
        })
        .catch(err);
    });
  }

  protected _refreshToken(): Promise<HttpParams> {
    if (!(<any>window).gapi) {
      setTimeout(() => this.refreshToken(), 500);
      return new Promise(ok => ok(null));
    }

    return this.auth2Load().then(auth2 => {
      const googleUser = auth2.currentUser.get();

      return googleUser.reloadAuthResponse()
        .then(res => {
          this._token = res.access_token;
          return this._getParamsForSite();
        });
    });
  }

  protected _logout() : Promise<void> {
    return this.auth2Load(false)
      .then(auth2 => {
        const gapi = (<any>window).gapi;
        gapi.auth2.getAuthInstance().disconnect();
        return;
      });
  }

  protected _getParamsForSite() : HttpParams {
    return new HttpParams()
      .set('grant_type', 'social_network')
      .set('identity_provider', authorizationServiceType.google)
      .set('assertion', this._token);
  }

  private loadSDK() {
    (function(d, s, id) {
      if (d.getElementById(id)) {
        return;
      }

      const js: any = d.createElement(s); js.id = id;
      js.src = 'assets/google-platform.js';

      const fjs = d.getElementsByTagName(s)[0];
      fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'google-sdk'));
  }

  private auth2Load(isAuthorization = true) : Promise<any> {
    return new Promise((ok, err) => {
      try {
        const gapi = (<any>window).gapi;
        gapi.load('auth2', {
          callback: function() {
            const params = {
              client_id: AppConfig.googleApiKey,
            };

            if(isAuthorization) {
              params['fetch_basic_profile'] = true;
              params['scope'] = 'profile email https://www.googleapis.com/auth/plus.login';
            }

            try {
              const auth2 = gapi.auth2.getAuthInstance() || gapi.auth2.init(params);
              ok(auth2);
            }
            catch (error2) {
              err(error2);
            }
          },
          onerror: function() {
            err('gapi.client failed to load!');
          },
          timeout: 5000, // 5 seconds.
          ontimeout: function() {
            err('gapi.client could not load in a timely manner!');
          }
        });
      }
      catch (error) {
        err(error);
      }
    });
  }
}



