﻿import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';
import { TypeOS } from '../enums/TypeOS';
import { Logger } from '../Logger';

const { execFile } = require('child_process');
const os = require('os');
const fixPath = require('fix-path');

const osType = os.type();
const isMac = osType === TypeOS.Darwin;
const isWindows = osType === TypeOS.Windows_NT;
const isLinux = osType === TypeOS.Linux;

@Injectable()
export class MinerService implements OnDestroy{

  private _isActiveServer = new BehaviorSubject<boolean>(false);
  private _isOnMining = new BehaviorSubject<boolean>(false);
  private _isProcessingMiner = new BehaviorSubject<boolean>(false);

  private _intervalPing = null;
  private _isServerRun = false; //for control work api server -> and restart if fell
  private _isWasResponse = false;

  public isActiveServer$ = this._isActiveServer.asObservable();
  public isOnMining$ = this._isOnMining.asObservable();
  public isProcessingMiner$ = this._isProcessingMiner.asObservable();

  constructor(private http: HttpClient,
              private userService: UserService,
              private ngzone: NgZone) {
    this.pingMinerApiServer = this.pingMinerApiServer.bind(this);
    this.pingMinerApiServer();
    this._intervalPing = setInterval(this.pingMinerApiServer, 3000);
  }

  ngOnDestroy(){
    clearInterval(this._intervalPing);
  }

  public static get SeparatorPath(){
    return isWindows ? '\\' : '/';
  }

  public static get PathToMiner() {
    let pathToApp = __dirname;

    if(pathToApp.indexOf('node_modules') > 0){
      pathToApp = pathToApp.substr(0, pathToApp.indexOf('node_modules'));
    }
    if(pathToApp.indexOf('electron.asar') > 0){
      pathToApp = pathToApp.substr(0, pathToApp.indexOf('electron.asar'));
    }

    if(AppConfig.environment === 'LOCAL'){
      pathToApp = pathToApp.substr(0, pathToApp.indexOf('PlayerClient'));
    }

    pathToApp += 'miner' + MinerService.SeparatorPath;

    Logger.log('PathToMiner="' + pathToApp + '"');
    return pathToApp;
  }

  public set IsActiveServer(value: boolean){
    this.ngzone.run(() => this._isActiveServer.next(value));
  }
  public set IsOnMining(value: boolean){
    this.ngzone.run(() => this._isOnMining.next(value));
  }

  public startMinerApiServer(){
    this._isWasResponse = false;
    this._isProcessingMiner.next(true);

    return new Promise((ok, err) => {

      let dopProcess = '';

      //fix mac Path
      if(isMac){
        fixPath();
      }

      if(isWindows){
        dopProcess = '.exe';
      }

      let PathToApiServerMiner = MinerService.PathToMiner;

      if(AppConfig.environment === 'LOCAL'){
        PathToApiServerMiner += (isMac ? 'mac64' : 'win64') + MinerService.SeparatorPath;
      }

      this._isServerRun = true;
      execFile(PathToApiServerMiner + 'ApiControlMiner' + dopProcess,  [], (error, stdout, stderr) =>  {
        if (error) {
          Logger.log(`execFile error: ${error}`);
          err(error);
        }
      });

      this.pingMinerApiServer()
        .then(res => {
          this._isWasResponse = true;
          ok();
        })
        .catch(res => {
          this._isWasResponse = true;
          err();
        });
    });
  }

  public pingMinerApiServer(){
    return this.http.post(AppConfig.minerApiServerUrl + '/api/control-mining/ping', null)
      .toPromise()
      .then((res: boolean) => {
          this.IsActiveServer = true;
          this.IsOnMining = res;
          if(this._isWasResponse){
            this._isProcessingMiner.next(false);
            this._isWasResponse = false;
          }
      })
      .catch(err => {
        this.IsActiveServer = false;
        if(this._isServerRun){ //restart
          this.startMinerApiServer();
        }
        if(this._isWasResponse){
          this._isProcessingMiner.next(false);
          this._isWasResponse = false;
        }
      });
  }

  public startMiner(token){
    this._isWasResponse = false;
    this._isProcessingMiner.next(true);

    const data = {
      LoginUser: this.userService.getUser().workerId,
      Token: token,
      WorkingDirectory: MinerService.PathToMiner,
      IsProduction: AppConfig.production
    };

    return this.http.post(AppConfig.minerApiServerUrl + '/api/control-mining/start', data)
      .toPromise()
      .then(() => {
        this._isWasResponse = true;
        this.pingMinerApiServer();
      })
      .catch(err => {
        this._isWasResponse = true;
        Logger.log('error start miner: ', err);
        this.IsOnMining = false;
        throw typeof err.error === 'string' ? err.error : err.error[0];
      });
  }

  public stopMiner(){
    this._isWasResponse = false;
    this._isProcessingMiner.next(true);

    return this.http.post(AppConfig.minerApiServerUrl + '/api/control-mining/stop', null)
      .toPromise()
      .then(() => {
        this._isWasResponse = true;
        this.pingMinerApiServer();
      })
      .catch(err => {
        this._isWasResponse = true;
        Logger.log('error stop miner: ', err);
      });
  }

  public stopMinerApiServer(){
    this.IsActiveServer = false;
    this.IsOnMining = false;
    this._isServerRun = false;

    return this.http.post(AppConfig.minerApiServerUrl + '/api/control-mining/off-server', null)
      .toPromise()
      .catch(this.pingMinerApiServer);
  }
}



