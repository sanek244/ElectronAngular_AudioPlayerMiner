﻿import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class ConfirmService{


  private _isOpen = new BehaviorSubject<boolean>(false);
  private _text = new BehaviorSubject<string>('');

  public callbackOk: Function = () => {};
  public isOpen$ = this._isOpen.asObservable();
  public text$ = this._text.asObservable();

  constructor() {}

  public open(text, callbackOk){
    this._isOpen.next(true);
    this._text.next(text);
    this.callbackOk = callbackOk;
  }

  public close(){
    this._isOpen.next(false);
    this._text.next('');
  }
}
