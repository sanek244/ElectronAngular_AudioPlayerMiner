﻿import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/internal/operators';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()
export class RoutingService{
  private _history = new BehaviorSubject<string[]>([]);
  private _indexHistory = new BehaviorSubject<number>(-1);

  public indexHistory$ = this._indexHistory.asObservable();

  constructor(private router: Router) {}

  public get indexHistory(): number {
    return this._indexHistory.getValue();
  }

  public get history() : string[] {
    return this._history.getValue();
  }

  public loadRouting(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({urlAfterRedirects}: NavigationEnd) => {
        //navigate on old address
        if(this.history[this.indexHistory] === urlAfterRedirects){
          return;
        }

        let history = this.history;

        //if navigate by history and change url (click to new link) then remove all urls after index url and add new url
        if(this.indexHistory !== this.history.length - 1) {
          history = this.history.slice(0, this.indexHistory + 1);
        }

        const newRoute =[...history, urlAfterRedirects];

        this._history.next(newRoute);
        this._indexHistory.next(newRoute.length - 1);
        return;
      });
  }

  get isCanPrev(): boolean{
    return this.indexHistory > 0;
  }
  get isCanNext(): boolean{
    return this.indexHistory < this.history.length - 1;
  }

  public goToPrevUrl(){
    if(this.isCanPrev){
      this._indexHistory.next(this.indexHistory - 1);
      this.router.navigateByUrl(this.history[this.indexHistory]);
    }
  }

  public goToNextUrl(){
    if(this.isCanNext){
      this._indexHistory.next(this.indexHistory + 1);
      this.router.navigateByUrl(this.history[this.indexHistory]);
    }
  }
}
