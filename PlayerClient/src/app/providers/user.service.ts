﻿import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../environments/environment';

@Injectable()
export class UserService {

  private _user = new BehaviorSubject<User|null>(this.getUserFromStorage());

  public user$ = this._user.asObservable();

  constructor(private http: HttpClient,
              private ngzone: NgZone) {
  }

  private getUserFromStorage(){
    const user = JSON.parse(localStorage.getItem('user') || 'null');
    if(user && !('serviceAuthorizationName' in user)){
      localStorage.setItem('user', 'null');
      return null;
    }


    return user;
  }

  getUser(){
    return this._user.getValue();
  }

  setUser(value: User|null){
    localStorage.setItem('user', JSON.stringify(value));

    this.ngzone.run(()=> this._user.next(value));
  }

  getDataFromServer(userToken){
    const header = {
      headers: new HttpHeaders({
        'Authorization':  'Bearer ' + userToken
      })
    };

    return this.http.get(AppConfig.siteUrl + '/api/Account/users/me', header).toPromise();
  }
}


export enum authorizationServiceType {
  google = 'google',
  facebook = 'facebook',
  twitter = 'twitter',
  locale = 'locale',
}





