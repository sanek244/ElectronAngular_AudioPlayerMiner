﻿﻿import {Injectable, NgZone} from '@angular/core';
import {BehaviorSubject } from 'rxjs';
import {Helper} from '../Helper';
import {Music} from '../models/Music';


@Injectable()
export class MusicService {

  private _musicQueue = new BehaviorSubject<Music[]>([]);
  private _indexQueue = new BehaviorSubject<number>(0);
  private _isRepeat = new BehaviorSubject<boolean>(false);
  private _isShuffle = new BehaviorSubject<boolean>(false); //mode: next song is random from the queue
  private _isOpenQueueWindow = new BehaviorSubject<boolean>(false);
  private _musicCurrent = new BehaviorSubject<Music|null>(null);
  private _musicQueueOrder = []; //for return from random mode
  private _isPlayCurrentMusic = new BehaviorSubject<boolean>(false);


  public musicQueue$ = this._musicQueue.asObservable();
  public musicCurrent$ = this._musicCurrent.asObservable();
  public isOpenQueueWindow$ = this._isOpenQueueWindow.asObservable();
  public isPlayCurrentMusic$ = this._isPlayCurrentMusic.asObservable();

  constructor(private ngZone: NgZone) {
    this.changeCurrentMusicFromIndexQueue = this.changeCurrentMusicFromIndexQueue.bind(this);
  }

  public set isPlayCurrentMusic(newValue: boolean){
    if(newValue !== this._isPlayCurrentMusic.getValue()){
      this._isPlayCurrentMusic.next(newValue);
    }
  }

  public set isRepeat(newValue: boolean){
    this._isRepeat.next(newValue);
  }
  public get isRepeat() : boolean{
    return this._isRepeat.getValue();
  }

  public set isShuffle(newValue: boolean){
    this._isShuffle.next(newValue);

    if(this.isShuffle){
      this._mixQueue();
    }
    else{
      const keys = {};
      this._musicQueue.getValue().map(el => keys[el.id] = el);
      const array = this._musicQueueOrder.map(id => keys[id]);
      this._musicQueue.next(array);
    }
  }
  public get isShuffle() : boolean{
    return this._isShuffle.getValue();
  }

  public set isOpenQueueWindow(newValue: boolean){
    this.ngZone.run(() => this._isOpenQueueWindow.next(newValue));
  }
  public get isOpenQueueWindow() : boolean{
    return this._isOpenQueueWindow.getValue();
  }

  private _mixQueue(){
    this._musicQueue.next(Helper.shuffle(this._musicQueue.getValue()));
    const index = this._indexQueue.getValue();
    const musicId = this._musicQueueOrder[index];
    this._indexQueue.next(this._musicQueue.getValue().findIndex(x => x.id === musicId));
  }

  public changeCurrentMusic(music: Music){
    const currentMusic = this._musicCurrent.getValue();

    if(currentMusic && currentMusic.id === music.id){
      this._isPlayCurrentMusic.next(!this._isPlayCurrentMusic.getValue());
      return;
    }

    this._isPlayCurrentMusic.next(true);
    this._musicCurrent.next(music);

    const newIndex = this._musicQueue.getValue().findIndex(x => x.id === music.id);
    this._indexQueue.next(newIndex);
  }
  public changeCurrentMusicFromIndexQueue(newIndex){
    const music = this._musicQueue.getValue();
    this._musicCurrent.next(newIndex > -1 && newIndex < music.length ? music[newIndex] : null);
  }

  public nextMusic(){
    this.changeIndexQueue(true);
  }
  public prevMusic(){
    this.changeIndexQueue(false);
  }
  private changeIndexQueue(isAdd){
    const musicQueue = this._musicQueue.getValue();
    if(musicQueue.length === 0){
      this._musicCurrent.next(null);
      this._indexQueue.next(0);
      return;
    }

    let newIndex = this._indexQueue.getValue() + (isAdd ? 1 : -1);

    //border queue
    let isStop = false;
    if(newIndex >= musicQueue.length){
      newIndex = this.isRepeat ? 0 : musicQueue.length - 1;
      isStop = !this.isRepeat;
    }
    else if(newIndex < 0){
      newIndex = this.isRepeat ? musicQueue.length - 1 : 0;
      isStop = !this.isRepeat;
    }

    if(isStop){
      this._isPlayCurrentMusic.next(false);
      return;
    }

    if(newIndex !== this._indexQueue.getValue()){
      this._indexQueue.next(newIndex);
      this._isPlayCurrentMusic.next(true);
      this.changeCurrentMusicFromIndexQueue(this._indexQueue.getValue());
    }
  }


  public addToQueue(music: Music | Music[]){

    music = [].concat(music).filter(x => !x.isExistInQueue);
    const arr = this._musicQueue.getValue().concat(music);
    this._musicQueueOrder = this._musicQueueOrder.concat(music.map(el => el.id));
    arr.map(x => x.isExistInQueue = true);
    this._musicQueue.next(arr);

    if(this.isShuffle){
      this._mixQueue();
    }
  }
  public removeFromQueue(music: Music){
    const arr = this._musicQueue.getValue().filter(x => x.id !== music.id);
    this._musicQueueOrder = this._musicQueueOrder.filter(x => x.id !== music.id);
    music.isExistInQueue = false;
    this._musicQueue.next(arr);
  }
  public changeQueue(musicArray: Music[], isRunMixShuffle = true){
    this._musicQueue.next([].concat(musicArray));
    this._musicQueueOrder = musicArray.map(el => el.id);

    if(isRunMixShuffle && this.isShuffle){
      this._mixQueue();
    }
  }
}

