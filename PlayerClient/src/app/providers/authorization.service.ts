﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {UserService} from './user.service';
import {AppConfig} from '../../environments/environment';
import {Logger} from '../Logger';
import {User} from '../models/User';
import {MinerService} from './miner.service';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export abstract class AuthorizationService{

  protected isExistInternet = navigator.onLine;

  constructor(protected userService: UserService,
              protected router: Router,
              protected http: HttpClient,
              protected minerService: MinerService,
              protected toastr: ToastrService) {
    this.init();
    this._processErrors = this._processErrors.bind(this);
  }

  protected init(){}

  /**
   * loginLogicBefore + onSuccess
   * @return {Promise<any>}
   */
  protected _loginLogic() : Promise<any> {
    //check exist internet
    if(!navigator.onLine){
      return new Promise((ok, err) => err('No connection to the Internet!'));
    }

    return this.http.get('https://www.googleapis.com/plus/v1/people')
      .toPromise()
      .catch(err => {
        if(err.status === 400){
          return this._login()
            .then(httpParams => this._onSuccessAuthOrRefreshToken(httpParams, true))
            .catch(this._processErrors);
        }

        throw 'No connection to the Internet!';
      });
  }
  protected _login(): Promise<HttpParams> {
    return new Promise(ok => ok(this._getParamsForSite()));
  }


  public refreshToken(): Promise<string> {
    return this._refreshToken()
      .then(httpParams => this._onSuccessAuthOrRefreshToken(httpParams, false));
  }
  protected _refreshToken(): Promise<HttpParams> {
    return new Promise(ok => ok(this._getParamsForSite()));
  }


  public logout() : Promise<void> {
    return this._logout()
      .then(() => {
        this.minerService.stopMiner();
        this.userService.setUser(null);
        this.router.navigate(['/authorization']);
        return;
      });
  }
  protected _logout() : Promise<void> {
    return new Promise((ok, err) => ok());
  }


  /**
   * For authorization and refresh token
   * logic redirect + get site token + get user data from site
   * @param {HttpParams} httpParams
   * @param isAuthorization
   * @return {Promise<string>}
   * @private
   */
  protected _onSuccessAuthOrRefreshToken(httpParams: HttpParams, isAuthorization): Promise<string> {
    if(!httpParams){
      Logger.log('_onSuccessAuthOrRefreshToken', 'empty httpParams');
      return;
    }

    //upp speed authorization for social networks
    if(isAuthorization){
      this.router.navigate(['/']);
    }

    return this._getTokenSite(httpParams)
      .then((token: any) => {
        const user = this.userService.getUser() || new User();
        user.token = token.access_token;
        this.userService.setUser(user);

        if (!user.workerId) {
          return this.userService.getDataFromServer(user.token)
            .then((dataUser: any) => {
              user.workerId = dataUser.workerId;

              this.userService.setUser(user);

              return token.access_token;
            });
        }

        return token.access_token;
      })
      .catch(err => {
        if(err.error.error === 'invalid_grant'){
          this.userService.setUser(null);
          if(location.pathname === '/'){
            this.router.navigate(['/authorization']);
            return;
          }
        }

        throw err;
      });
  }

  protected _getTokenSite(data: HttpParams) {

    const header = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post(AppConfig.siteUrl + '/api/connect/token', data.toString(), header)
      .toPromise()
      .catch(err => {
        Logger.log('error getToken from server site', err);
        throw err;
      });
  }

  protected _getParamsForSite() : HttpParams {
    return new HttpParams();
  }

  protected _processErrors(err){
    Logger.err(err);

    if(typeof err === 'object'){

      if('length' in err){
        err = err[0];
      }

      if('status' in err){
        if(err.status === 404){
          throw 'The server is temporarily unavailable! Please try again later.';
        }

        if(err.status === 500){
          throw 'There was an error on the server, please try again later.';
        }

        if(err.status === 0){
          throw 'No connection to the server authorization!';
        }
      }

      if('error' in err){
        err = err.error ? err.error : err.message || 'Unknown error.';
      }

      if(typeof err === 'object'){
        if('length' in err){
          return this._processErrors(err[0]);
        }

        if('error_description' in err){
          throw err.error_description;
        }
        else{
          throw 'Error, please try again later!';
        }
      }

      if(err === 'popup_closed_by_user'){
        return;
      }
    }

    throw err;
  }
}

export interface IAuthorizationService{
  login: () => Promise<HttpParams>;
  refreshToken: () => Promise<HttpParams>;
  logout: () => Promise<void>;
}


