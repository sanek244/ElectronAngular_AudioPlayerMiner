﻿import {Music} from '../models/Music';

﻿import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Playlist} from '../models/Playlist';

@Injectable()
export class MusicPlaylistService {

  private _playlists = new BehaviorSubject<Playlist[]>(this.loadPlaylists());
  private _isOpenModalCreate = new BehaviorSubject<boolean>(false);

  public playlists$ = this._playlists.asObservable();
  public isOpenModalCreate$ = this._isOpenModalCreate.asObservable();
  public musicAddedAfterCreate: Music[] = [];

  constructor() {
  }

  private loadPlaylists(){
    return JSON.parse(localStorage.getItem('playlists') || '[]')
      .map((playlist: Playlist) => new Playlist().parse(playlist));
  }

  public openModalCreate(musicAdded: Music[] = []){
    this.musicAddedAfterCreate = musicAdded;
    this._isOpenModalCreate.next(true);
  }

  public closeModalCreate(){
    this._isOpenModalCreate.next(false);
    this.musicAddedAfterCreate = [];
  }

  public addPlaylist(name: string, description: string, imgaePath: string){
    const newPlaylist = new Playlist(name, description, imgaePath);
    newPlaylist.music = this.musicAddedAfterCreate;
    this.musicAddedAfterCreate = [];
    this._playlists.next([...this._playlists.getValue(), newPlaylist]);
    this._savePlaylists();
  }

  public deletePlaylist(id){
    this._playlists.next(this._playlists.getValue().filter(x => x.id !== id));
    this._savePlaylists();
  }

  private _savePlaylists(){
    localStorage.setItem('playlists', JSON.stringify(this._playlists.getValue()));
  }

  public get playlists(){
    return this._playlists.getValue();
  }

  public getPlaylistById(id: string){
    return this.playlists.find(x => x.id === id);
  }

  public addMusicToPlaylist(idPlaylist: string, music: Music){
    this.getPlaylistById(idPlaylist).music.push(music);
    this._savePlaylists();
  }

  public removeMusicFromPlaylist(idPlaylist: string, idMusic: string){
    const playlist = this.getPlaylistById(idPlaylist);
    playlist.music = playlist.music.filter(x => x.id !== idMusic);
    this._savePlaylists();
  }
}
