﻿import {Injectable } from '@angular/core';
import {User} from '../models/User';
import {authorizationServiceType} from './user.service';
import {AuthorizationService } from './authorization.service';
import {AppConfig} from '../../environments/environment';
import {Logger} from '../Logger';
import {HttpParams} from '@angular/common/http';
import {Helper} from '../Helper';

const OAuth = require('oauth').OAuth;
const nodeUrl = require('url');

@Injectable()
export class AuthorizationTwitterService extends AuthorizationService{

  public login() : Promise<any> {
    return super._loginLogic();
  }

  protected _login(): Promise<HttpParams> {
    return this.getRequestToken()
      .then(res => {
        const win = Helper.openWindow('https://twitter.com/oauth/authorize?oauth_token=' + res[0], 'twitter', 800, 700);
        win.focus();

        //wait authorization in new window
        return new Promise<HttpParams>(ok => {
          const interval = setInterval(() => {
            if(win.location == null){
              clearInterval(interval);
              ok(null);
            }
            else if((win.location + '').indexOf('://localhost:') > -1){
              const url = win.location + '';

              win.close();
              clearInterval(interval);

              const urlObj = nodeUrl.parse(url, true);
              this.callbackLogin(urlObj.query.oauth_token, urlObj.query.oauth_verifier)
                .then(res => ok(res));
            }
          }, 500);
        });
      });
  }

  private callbackLogin(oauth_token, oauth_verifier) : Promise<HttpParams>{
    return this.getUserData(oauth_token, oauth_verifier)
      .then(res => {
        const profile = res[0];
        const user = new User();
        user.familyName = '';
        user.givenName = profile['screen_name'];
        user.fullName = profile['name'];
        user.imageUrl = profile['profile_image_url'];
        user.email = profile['email'];
        user.tokenSocialNetwork = res[1];
        user.serviceAuthorizationName = authorizationServiceType.twitter;
        user.id = profile['id'];
        this.userService.setUser(user);

        return this._getParamsForSite();
      });
  }

  protected _getParamsForSite() : HttpParams{
    const user = this.userService.getUser();
    return new HttpParams()
      .set('grant_type', 'social_network')
      .set('identity_provider', authorizationServiceType.twitter)
      .set('assertion', user.tokenSocialNetwork)
      .set('twitter_name', user.fullName)
      .set('twitter_id', user.id)
      .set('twitter_email', user.email);
  }

  private getUserData(oauth_token, oauth_verifier){
    return new Promise((ok, err) => {
      this.getAccessToken(oauth_token, oauth_verifier)
        .then(res => {
          this.oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true', res[0], res[1], (error, twitterResponseData, result) => {
            if(error){
              Logger.err('twitter', 'callbackLogin');
              err(this.formatErrors(error));
              return;
            }

            try {
              const data = JSON.parse(twitterResponseData);
              ok([data, res[0]]);
            }
            catch (parseError) {
              err(parseError);
              return;
            }
          });
        })
        .catch(error => err(error));
    });
  }

  private formatErrors(error){
    Logger.err('twitter', 'login', error);

    let errorMessage = error.data;
    if(errorMessage.indexOf('<errors>') === -1){
      return errorMessage;
    }

    errorMessage = errorMessage.substr(errorMessage.indexOf('<errors>') + '<errors>'.length);
    errorMessage = errorMessage.substr(0, errorMessage.indexOf('</errors>'));
    return errorMessage.split('error')
      .map(x => {
        let res = x.substr(x.indexOf('">') + 2);
        res = res.substr(0, res.indexOf('<'));
        return res;
      })
      .filter(x => x);
  }

  private get oauth(){
    return new OAuth(
        'https://api.twitter.com/oauth/request_token',
        'https://api.twitter.com/oauth/access_token',
        AppConfig.TWITTER_CONSUMER_KEY,
        AppConfig.TWITTER_CONSUMER_SECRET,
        '1.0',
        'http://localhost:4200/?auth=twitter',
        'HMAC-SHA1'
    );
  }

  private getRequestToken() : Promise<string[]> {
    return new Promise((ok, err) => {
      this.oauth.getOAuthRequestToken(function (error, oAuthToken, oAuthTokenSecret, results) {
        if(error){
          Logger.err('twitter', 'getRequestToken');
          err(this.formatErrors(error));
          return;
        }

        ok([oAuthToken, oAuthTokenSecret, results]);
      });
    });
  }

  private getAccessToken(oauth_token, oauth_verifier) : Promise<string[]> {
    return new Promise((ok, err) => {
      this.getRequestToken()
        .then(res => {
          this.oauth.getOAuthAccessToken(oauth_token, res[0], oauth_verifier, (error, oAuthAccessToken, oAuthAccessTokenSecret, results) => {
              if(error){
                Logger.err('twitter', 'getAccessToken');
                err(this.formatErrors(error));
                return;
              }

              ok([oAuthAccessToken, oAuthAccessTokenSecret, results]);
          });
        })
        .catch(error => err(error));
    });
  }
}



