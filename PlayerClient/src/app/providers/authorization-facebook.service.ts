﻿import {Injectable } from '@angular/core';
import {User} from '../models/User';
import {authorizationServiceType} from './user.service';
import {AuthorizationService } from './authorization.service';
import {AppConfig} from '../../environments/environment';
import {HttpParams} from '@angular/common/http';
import {Helper} from '../Helper';
import {Logger} from '../Logger';

const nodeUrl = require('url');

@Injectable()
export class AuthorizationFacebookService extends AuthorizationService{

  private _FB;
  private _intervalCheckWindow;
  private _window : any;

  init() {
    this.callbackMessageWindowPost = this.callbackMessageWindowPost.bind(this);
    this.loadSDK();
  }

  public login() : Promise<any> {
    return super._loginLogic();
  }

  protected _login(): Promise<HttpParams> {
    if(this._window){
      clearInterval(this._intervalCheckWindow);
      this._window.close();
      this._window = null;
    }

    const data = {
      client_id: AppConfig.facebookAppId,
      scope: 'public_profile,email',
      response_type: 'token',
      auth_type: 'reauthenticate',
      redirect_uri: 'http://localhost:4200/',
      display: 'popup'
    };

    const url = 'https://www.facebook.com/v3.1/dialog/oauth?' + Object.keys(data).map(key => key + '=' + data[key]).join('&');
    this._window = Helper.openWindow(url, 'facebook', 600, 600);

    //add button logout
    this._window.eval('document.getElementsByTagName("body")[0].innerHTML += "<iframe id=\'iFrame_logout_button\' name=\'f8071728c594\' height=\'1000px\' frameborder=\'0\' allowtransparency=\'true\' allowfullscreen=\'true\' scrolling=\'no\' allow=\'encrypted-media\' title=\'fb:login_button Facebook Social Plugin\' src=\'https://www.facebook.com/v3.1/plugins/login_button.php?app_id=196228561018542&amp;auto_logout_link=true&amp;button_type=continue_with&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FQX17B8fU-Vm.js%3Fversion%3D42%23cb%3Df27bdf5895b599%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%253A4200%252Ff564455162fd0c%26relation%3Dparent.parent&amp;container_width=0&amp;locale=en_US&amp;max_rows=1&amp;sdk=joey&amp;show_faces=false&amp;size=medium&amp;use_continue_as=false\' style=\'border: none;visibility: visible;width: 95px;height: 28px;position:absolute;top:3px;right:0;\' ></iframe>"');
    //show button logout only when exist user in cookie (c_user > 0)
      //+ if log out -> close window (location.reload() and change loaction.href -> not working)
    this._window.eval('if(document.cookie.indexOf("c_user") === -1 && document.cookie.indexOf("c_user=0") === -1){' +
        'document.getElementById(\'iFrame_logout_button\').style.display = "none"' +
      '}' +
      'else {' +
        'setInterval(() => {' +
          'const isReload = document.cookie.indexOf("c_user") === -1 && document.cookie.indexOf("c_user=0") === -1;' +
          'if(isReload){window.opener.postMessage(\'facebook_logout\', \'*\'); window.close();}' +
        '}, 300)' +
      '}');

    //TODO: for dev time - after remove;
    //clear cookie on key esc (such cleaning cookie causes a bug of facebook, because of which it is impossible to authorize. You need to restart the application)
    this._window.eval('document.onkeyup = event => {if(event.keyCode === 27){document.cookie = \'c_user=0\'; location.reload(); window.opener.postMessage(\'facebook_restart\', \'*\');}}');

    //wait authorization in new window
    return new Promise<HttpParams>(ok => {
      this._intervalCheckWindow = setInterval(() => {
        if(this._window.location == null){
          this._window = null;
          clearInterval(this._intervalCheckWindow);
          ok(null);
        }
        else if((this._window.location + '').indexOf('://localhost:') > -1 && (this._window.location + '').indexOf('://localhost:') < 7){
          const url = this._window.location + '';

          this._window.close();
          this._window = null;
          clearInterval(this._intervalCheckWindow);
          const urlObj = nodeUrl.parse(url.replace('#', ''), true);
          this.callbackLogin(urlObj.query.access_token)
            .then(res => ok(res));
        }
      }, 500);
    });
  }

  private callbackLogin(access_token) : Promise<HttpParams>{
    if(!this._FB && !this.isExistInternet){
      Logger.log('facebook download...');
      this.loadSDK();
    }

    return new Promise(ok => {
      this._FB.api('/me', {access_token: access_token, fields: 'last_name,name,email,picture'}, (response) => {
        const user = new User();
        user.familyName = response.last_name;
        user.givenName = response.name;
        user.fullName = response.name;
        user.imageUrl = response.picture.data.url;
        user.email = response.email;
        user.tokenSocialNetwork = access_token;
        user.serviceAuthorizationName = authorizationServiceType.facebook;
        this.userService.setUser(user);

        ok(this.getParamsForSite(user.tokenSocialNetwork));
      });
    });
  }

  protected _refreshToken(): Promise<HttpParams> {
    return new Promise((ok, err) =>
      ok(this.getParamsForSite(this.userService.getUser().tokenSocialNetwork)));
  }

  protected getParamsForSite(token) : HttpParams{
    return new HttpParams()
      .set('grant_type', 'social_network')
      .set('identity_provider', authorizationServiceType.facebook)
      .set('assertion', token);
  }

  private loadSDK(){
    (<any>window).fbAsyncInit = () => {
      this._FB = (<any>window).FB;
      this._FB.init({
        appId            : AppConfig.facebookAppId,
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v3.1'
      });
    };

    (function(d, s, id) {
      if (d.getElementById(id)) {
        return;
      }

      const js: any = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';

      const fjs = d.getElementsByTagName(s)[0];
      fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-sdk'));

    window.addEventListener('message', this.callbackMessageWindowPost, false);
  }

  private callbackMessageWindowPost(event){
    if(event.data === 'facebook_logout'){
      this.login();
    }

    if(event.data === 'facebook_restart'){
      const remote = require('electron').remote;
      remote.app.relaunch();
      remote.app.exit(0);
    }
  }

}



