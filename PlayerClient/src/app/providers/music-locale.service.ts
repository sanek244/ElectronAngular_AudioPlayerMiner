﻿import {Logger} from '../Logger';
﻿import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {MusicMetaData} from '../models/MusicMetaData';
import {Music} from '../models/Music';
import {TypeMusicSourceExternal} from '../enums/TypeMusicSource';

const jsmediatags = require('jsmediatags');
const glob = require('glob');

@Injectable()
export class MusicLocaleService {

  //selected music folders from the current computer
  private _sources = new BehaviorSubject<string[]>(JSON.parse(localStorage.getItem('sources') || '[]'));
  //music from localeSources
  private _music = new BehaviorSubject<Music[]>([]);

  public sources$ = this._sources.asObservable();
  public music$ = this._music.asObservable();

  constructor() {
    this.getMusicListFromSources = this.getMusicListFromSources.bind(this);

    this._sources.subscribe(this.getMusicListFromSources);
  }

  public get music() : Music[] {
    return this._music.getValue();
  }

  public getMetaData(music: Music) : Promise<MusicMetaData> {
    const path = music.path;
    return new Promise((ok, err) =>
      new jsmediatags.Reader(path)
        .setTagsToRead(['title', 'artist', 'album', 'picture'])
        .read({
          onSuccess: res => ok(new MusicMetaData(res.tags, path)),
          onError: error => Logger.err('getMusicCurrentInfo', error)
        }));
  }


  public addSource(source: string) {
    const sources = this._sources.getValue();
    sources.push(source);
    this.setSources(sources);
  }

  public getSources(){
    return this._sources.getValue();
  }

  public setSources(value: string[]){
    localStorage.setItem('sources', JSON.stringify(value));
    this._sources.next(value);
  }

  public removeSource(source: string) {
    let sources = this._sources.getValue();
    sources = sources.filter(x => x !== source);
    this.setSources(sources);
  }

  public getMusicListFromSources(sources){
    this._music.next([]);

    const promises = sources.map(source => {
      glob(source + '/*.{mp3,wav,aiff,flac,ape,ogg}', (err, files) => {
        const music = files.map(file =>
          new Music(file, file.substr(source.length + 1), TypeMusicSourceExternal.locale));

        this._music.next(this.music.concat(music));
      });
    });
  }
}
