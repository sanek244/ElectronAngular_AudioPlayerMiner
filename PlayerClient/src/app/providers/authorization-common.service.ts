﻿import {Injectable } from '@angular/core';
import {authorizationServiceType, UserService} from './user.service';
import {AuthorizationTwitterService} from './authorization-twitter.service';
import {AuthorizationFacebookService} from './authorization-facebook.service';
import {AuthorizationGoogleService} from './authorization-google.service';
import {AuthorizationLocaleService} from './authorization-locale.service';

@Injectable()
export class AuthorizationCommonService {

  constructor(private userService: UserService,
              private authorizationGoogleService: AuthorizationGoogleService,
              private authorizationTwitterService: AuthorizationTwitterService,
              private authorizationLocaleService: AuthorizationLocaleService,
              private authorizationFacebookService: AuthorizationFacebookService) {
  }

  public refreshToken(){
    const user = this.userService.getUser();
    switch (user.serviceAuthorizationName){
      case authorizationServiceType.locale: return this.authorizationLocaleService.refreshToken();
      case authorizationServiceType.facebook: return this.authorizationFacebookService.refreshToken();
      case authorizationServiceType.twitter: return this.authorizationTwitterService.refreshToken();
      case authorizationServiceType.google: return this.authorizationGoogleService.refreshToken();
    }

    throw 'user.serviceAuthorizationName not recognized';
  }

  public logout(){
    const user = this.userService.getUser();
    switch (user.serviceAuthorizationName){
      case authorizationServiceType.locale: return this.authorizationLocaleService.logout();
      case authorizationServiceType.facebook: return this.authorizationFacebookService.logout();
      case authorizationServiceType.twitter: return this.authorizationTwitterService.logout();
      case authorizationServiceType.google: return this.authorizationGoogleService.logout();
    }

    throw 'user.serviceAuthorizationName not recognized';
  }

}



