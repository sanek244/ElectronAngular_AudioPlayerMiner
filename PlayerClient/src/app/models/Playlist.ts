import {Music} from './Music';
import {BaseModel} from './BaseModel';

export class Playlist extends BaseModel {
  title: string;
  description: string;
  imagePath: string;
  music: Music[];

  constructor(title = '', description = '', imagePath = ''){
    super();

    this.title = title || '';
    this.description = description || '';
    this.imagePath = imagePath || '';
    this.music = [];
  }

  parse(data: any){
    super.parse(data);

    this.music = this.music.map(music => new Music().parse(music));
    return this;
  }
}
