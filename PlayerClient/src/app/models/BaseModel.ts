import {Helper} from '../Helper';

export class BaseModel {
  id: string;

  constructor(){
    this.id = Helper.generateUid();
  }

  /**
   * Загрузка данных в модель
   * !!! Все поля должны быть обязательно инициализированы! иначе они пропадут. -> особенность TypeScript !!!
   * @param {object} data
   */
  public parse(data: any){
    if(data) {
      if(typeof data === 'object' && !('className' in data)){
        Object.keys(data).map(key => {
          if(key in this) {
            this[key] = data[key];
          }
        });
      }
    }

    return this;
  }
}
