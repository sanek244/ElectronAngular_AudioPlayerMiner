import {BaseModel} from './BaseModel';

export class Album extends BaseModel{
  idArtist: string;
  picture: string;
  title: string;

  constructor(title: string = '', idArtist: string = ''){
    super();

    this.title = title;
    this.idArtist = idArtist;
    this.picture = '';
  }
}
