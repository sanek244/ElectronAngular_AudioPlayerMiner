import {BaseModel} from './BaseModel';

export class Artist extends BaseModel {
  genres: string[];
  picture: string;
  name: string;

  constructor(name: string = '', genre: string = ''){
    super();

    this.name = name;
    this.genres = [genre];
    this.picture = '';
  }
}
