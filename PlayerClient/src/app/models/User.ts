import { authorizationServiceType } from '../providers/user.service';
import {BaseModel} from './BaseModel';

export class User extends BaseModel {
  email: string;
  fullName: string;
  givenName: string;
  familyName: string;
  imageUrl: string;
  dateOfBirth: Date;

  token: string;
  password: string;
  tokenSocialNetwork: string;
  serviceAuthorizationName: authorizationServiceType;

  workerId: string;

  constructor(){
    super();

    this.email = '';
    this.fullName = '';
    this.givenName = '';
    this.familyName = '';
    this.imageUrl = '';
    this.dateOfBirth = null;
    this.token = '';
    this.password = '';
    this.tokenSocialNetwork = '';
    this.serviceAuthorizationName = null;
  }
}
