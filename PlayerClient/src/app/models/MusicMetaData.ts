import {Helper} from '../Helper';
import {BaseModel} from './BaseModel';

const base64js = require('base64-js');

export class MusicMetaData extends BaseModel{
  title: string; //from metadata or filename
  titleFull: string; //artist + title
  album: string;
  artist: string;
  pictureData: string;
  timePlaySecond: number;
  genres: string[]; //for aws

  constructor(data: any = null, path: string = ''){
    super();

    data = data === null ? {} : data;

    let titleFromPath = path.split(new RegExp('\\/')).pop();
    titleFromPath = titleFromPath.substr(0, titleFromPath.lastIndexOf('.'));

    this.title = data['title'] || titleFromPath || '';

    this.album = data['album'] || '';
    if(this.album.toLowerCase() === this.title.toLowerCase()){
      this.album = '';
    }

    this.artist = data['artist'] || '';
    if(this.artist.toLowerCase() === this.title.toLowerCase()){
      this.artist = titleFromPath.split('-').length > 1 ? titleFromPath.split('-')[0] : '';
    }

    this.pictureData = data['picture'] ? base64js.fromByteArray(data['picture']['data']) : '';

    this.titleFull = (this.artist ? (this.artist + ' - ') : '') + this.title;

    this.timePlaySecond = 0;
    this.genres = [];
  }

  public get timePlay(){
    return parseInt(this.timePlaySecond / 60 + '', 10) + ':' + Helper.twoZero(this.timePlaySecond % 60);
  }
}
