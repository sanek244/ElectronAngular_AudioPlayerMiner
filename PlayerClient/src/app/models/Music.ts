import {MusicMetaData} from './MusicMetaData';
import {TypeMusicSourceExternal} from '../enums/TypeMusicSource';
import {BaseModel} from './BaseModel';
import {AppConfig} from '../../environments/environment';

export class Music extends BaseModel {
  metaData: MusicMetaData;
  path: string;
  isExistInQueue: boolean;
  type: TypeMusicSourceExternal;
  idAlbum: string; //for aws
  idArtist: string; //for aws

  constructor(path: string = '', title: string = '', type: TypeMusicSourceExternal = TypeMusicSourceExternal.locale)
  {
    super();

    this.path = path;
    this.metaData = new MusicMetaData(null, path);
    this.metaData.title = title;
    this.type = type;
    this.idAlbum = '';
    this.idArtist = '';
  }

  get PathUri() : string {
    if(this.type === TypeMusicSourceExternal.AWS){
      return AppConfig.AWS_S3Host + AppConfig.AWS_bucketName + '/' + this.path;
    }
    return 'file://' + this.path.replace('#', '%23'); //encodeURI and encodeURIComponent not help
  }

  parse(data: any){
    super.parse(data);

    this.metaData = new MusicMetaData().parse(data.metaData);
    return this;
  }
}
