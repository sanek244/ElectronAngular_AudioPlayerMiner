import { Component } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import {RoutingService} from './providers/routing.service';
import {MusicService} from './providers/music.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public electronService: ElectronService,
              private translate: TranslateService,
              private routingService: RoutingService,
              private musicService: MusicService)
  {
    routingService.loadRouting();
    translate.setDefaultLang('en');

    //Keys up
    window.onkeypress = event => {
      if(event.keyCode === 101){
        this.musicService.isOpenQueueWindow = !this.musicService.isOpenQueueWindow;
      }
    };
  }
}
