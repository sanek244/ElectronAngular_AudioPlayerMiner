import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';

// angular
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// npm modules
import { DragDropListModule } from '../assets/ng-drag-drop-list';
import { ToastrModule } from 'ngx-toastr';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';


// base app
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// services
import { UserService } from './providers/user.service';
import { MusicService } from './providers/music.service';
import { MinerService } from './providers/miner.service';
import { RoutingService } from './providers/routing.service';
import { ConfirmService } from './providers/confirm.service';
import { ElectronService } from './providers/electron.service';
import { MusicAWSService } from './providers/music-aws.service';
import { MusicLocaleService } from './providers/music-locale.service';
import { MusicPlaylistService } from './providers/music-playlist.service';
import { AuthorizationCommonService } from './providers/authorization-common.service';
import { AuthorizationGoogleService } from './providers/authorization-google.service';
import { AuthorizationLocaleService } from './providers/authorization-locale.service';
import { AuthorizationTwitterService } from './providers/authorization-twitter.service';
import { AuthorizationFacebookService } from './providers/authorization-facebook.service';

// directives
import { WebviewDirective } from './directives/webview.directive';

// pages
import { PlayerComponent } from './pages/player/player.component';
import { AuthorizationComponent } from './pages/authorization/authorization.component';

// components
import { ModalComponent } from './components/modal/modal.component';
import { UserBarComponent } from './components/user-bar/user-bar.component';
import { MenuLeftComponent } from './components/menu-left/menu-left.component';
import { MusicListComponent } from './components/music-list/music-list.component';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { ViewMusicComponent } from './components/view-music/view-music.component';
import { FormSearchComponent } from './components/form-search/form-search.component';
import { ViewGenresComponent } from './components/view-genres/view-genres.component';
import { ViewAlbumsComponent } from './components/view-albums/view-albums.component';
import { ViewArtistsComponent } from './components/view-artists/view-artists.component';
import { ViewSettingComponent } from './components/view-setting/view-setting.component';
import { ModalConfirmComponent } from './components/modal-confirm/modal-confirm.component';
import { MinerCheckboxComponent } from './components/miner-checkbox/miner-checkbox.component';
import { ViewAlbumMusicComponent } from './components/view-album-music/view-album-music.component';
import { ControlNavigateComponent } from './components/control-navigate/control-navigate.component';
import { MusicControlBarComponent } from './components/music-control-bar/music-control-bar.component';
import { FormRegistrationComponent } from './components/form-registration/form-registration.component';
import { MusicQueueWindowComponent } from './components/music-queue-window/music-queue-window.component';
import { ViewPlaylistMusicComponent } from './components/view-playlist-music/view-playlist-music.component';
import { CheckboxHorizontalComponent } from './components/checkbox-horizontal/checkbox-horizontal.component';
import { MusicLocaleSourcesComponent } from './components/music-locale-sources/music-locale-sources.component';
import { ModalCreatePlaylistComponent } from './components/modal-create-playlist/modal-create-playlist.component';
import { ModalForgotPasswordComponent } from './components/modal-forgot-password/modal-forgot-password.component';
import { AuthorizationSocialNetworksComponent } from './components/authorization-social-networks/authorization-social-networks.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    ModalComponent,
    UserBarComponent,
    WebviewDirective,
    MenuLeftComponent,
    ViewMusicComponent,
    FormLoginComponent,
    MusicListComponent,
    FormSearchComponent,
    ViewGenresComponent,
    ViewAlbumsComponent,
    ViewArtistsComponent,
    ViewSettingComponent,
    ModalConfirmComponent,
    AuthorizationComponent,
    MinerCheckboxComponent,
    ViewAlbumMusicComponent,
    MusicControlBarComponent,
    ControlNavigateComponent,
    FormRegistrationComponent,
    MusicQueueWindowComponent,
    ViewPlaylistMusicComponent,
    CheckboxHorizontalComponent,
    MusicLocaleSourcesComponent,
    ModalCreatePlaylistComponent,
    ModalForgotPasswordComponent,
    AuthorizationSocialNetworksComponent,
  ],
  imports: [
    NgbDropdownModule,
    FormsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DragDropListModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthorizationFacebookService,
    AuthorizationTwitterService,
    AuthorizationGoogleService,
    AuthorizationLocaleService,
    AuthorizationCommonService,
    MusicPlaylistService,
    MusicLocaleService,
    MusicAWSService,
    ElectronService,
    RoutingService,
    ConfirmService,
    MinerService,
    MusicService,
    UserService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
