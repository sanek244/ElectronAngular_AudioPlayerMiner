export class Helper{

  public static randomInt(min: number, max: number): number {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
  }

  /**
   * Uid
   * @return {string}
   */
  public static generateUid(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = Math.random()*16|0,
        v = c === 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  public static format(str: string, ...params: any[]) {
    return str.replace(/{(\d+)}/g, (match, number) => {
      if (params[number] === null) {
        return 'null';
      }
      if (typeof params[number] === 'undefined') {
        return match;
      }
      if (typeof params[number] === 'object') {
        return JSON.stringify(params[number]);
      }
      return params[number];
    });
  }

  public static last(arr: any[]){
    if(!arr || arr.length === 0){
      return null;
    }

    return arr[arr.length - 1];
  }

  public static openWindow(url, title, width, height) : Window {
    const left = (screen.width/2)-(width/2),
      top = (screen.height/2)-(height/2),
      win: any = window.open(url, title, Helper.format('width={0},height={1},top={2},left={3}', width, height, top, left));
    win.focus();

    return win;
  }

  public static twoZero(str: string | number) {
    const number = parseInt(str + '', 10);
    return number < 10 ? '0' + number : number;
  }

  public static shuffle(a : any[]) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  public static sortByField(array: any, fun: Function, isDESC = false) {
    return array.sort((a: any, b: any) => {
      if (fun(a) > fun(b)) {
        return isDESC ? 1 : -1;
      }

      if (fun(a) < fun(b)) {
        return isDESC ? -1 : 1;
      }

      return 0;
    });
  }
}
