import { Component, OnInit, OnDestroy } from '@angular/core';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {AuthorizationLocaleService} from '../../providers/authorization-locale.service';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit, OnDestroy {

  isSignIn: boolean = true;

  destroyed: Subject<any> = new Subject();

  constructor(private authorizationLocaleService: AuthorizationLocaleService) { }

  ngOnInit() {
    this.authorizationLocaleService.isSignIn$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isSignIn = res);
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }

  toggleSignIn(){
    this.authorizationLocaleService.isSignIn = !this.isSignIn;
  }
}
