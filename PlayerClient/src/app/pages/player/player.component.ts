import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Subject} from 'rxjs/index';
import { UserService } from '../../providers/user.service';
import { Router } from '@angular/router';
import { AuthorizationCommonService } from '../../providers/authorization-common.service';
import {MinerService} from '../../providers/miner.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  @ViewChild('contentDiv') contentDiv: ElementRef<HTMLDivElement>;
  @ViewChild('footerDiv') footerDiv: ElementRef<HTMLDivElement>;
  @ViewChild('topDiv') topDiv: ElementRef<HTMLDivElement>;
  @ViewChild('bodyDiv') bodyDiv: ElementRef<HTMLDivElement>;

  destroyed: Subject<any> = new Subject();

  constructor(private userService: UserService,
              private authorizationCommonService: AuthorizationCommonService,
              private router: Router,
              private minerService: MinerService) {
    this.onResize = this.onResize.bind(this);
  }

  ngOnInit() {
    if(!this.userService.getUser()){
      this.router.navigate(['/authorization']);
    }
    else{
      this.authorizationCommonService.refreshToken();
      this.minerService.startMinerApiServer();
    }

    setTimeout(this.onResize, 100);
  }

  onResize() {
    this.contentDiv.nativeElement.style.maxHeight =
      this.bodyDiv.nativeElement.clientHeight -
      this.footerDiv.nativeElement.clientHeight -
      this.topDiv.nativeElement.clientHeight - 2 + 'px';
  }

}
