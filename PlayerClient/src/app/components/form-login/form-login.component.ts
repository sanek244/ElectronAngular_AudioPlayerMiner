import {Component, OnInit} from '@angular/core';
import {AuthorizationLocaleService} from '../../providers/authorization-locale.service';
import {authorizationServiceType} from '../../providers/user.service';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})
export class FormLoginComponent implements OnInit {

  email: string;
  password: string;
  error: string;

  isLoginWait: boolean;

  constructor(private authorizationLocaleService: AuthorizationLocaleService) { }

  ngOnInit() {
  }

  openModalForgotPassword() {
    this.authorizationLocaleService.openModalForgotPassword();
  }

  login() {
    this.error = '';
    this.isLoginWait = true;
    this.authorizationLocaleService.login(this.email, this.password)
      .then(() => this.isLoginWait = false)
      .catch(error => {
        this.error = error;
        this.isLoginWait = false;
      });
  }

}
