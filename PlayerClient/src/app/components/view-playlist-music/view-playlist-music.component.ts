import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {Playlist} from '../../models/Playlist';
import {ActivatedRoute} from '@angular/router';
import {MusicPlaylistService} from '../../providers/music-playlist.service';
import {TypeMusicSourceExternal} from '../../enums/TypeMusicSource';
import {MusicService} from '../../providers/music.service';
import {Music} from '../../models/Music';
import {RoutingService} from '../../providers/routing.service';
import {Router} from '@angular/router';
import {ConfirmService} from '../../providers/confirm.service';

@Component({
  selector: 'app-view-playlist-music',
  templateUrl: './view-playlist-music.component.html',
  styleUrls: ['./view-playlist-music.component.scss']
})
export class ViewPlaylistMusicComponent implements OnInit, OnDestroy {

  playlist: Playlist;
  title: string = '';
  selectedMusicId : string = '';
  isPlay : boolean = false;
  destroyed: Subject<any> = new Subject();

  @ViewChild('image') image: ElementRef<HTMLDivElement>;
  @ViewChild('imageContainer') imageContainer: ElementRef<HTMLDivElement>;


  public constructor(private route: ActivatedRoute,
                     private musicPlaylistService: MusicPlaylistService,
                     private musicService: MusicService,
                     private routingService: RoutingService,
                     private confirmService: ConfirmService,
                     private router: Router)
  {
    this.changePlaylist = this.changePlaylist.bind(this);
    this._deletePlaylist = this._deletePlaylist.bind(this);
  }

  ngOnInit() {
    this.musicPlaylistService
      .playlists$
      .pipe(takeUntil(this.destroyed))
      .subscribe(this.changePlaylist);

    this.routingService
      .indexHistory$
      .pipe(takeUntil(this.destroyed))
      .subscribe(this.changePlaylist);

    this.musicService
      .musicCurrent$
      .pipe(takeUntil(this.destroyed))
      .subscribe(musicCurrent => this.selectedMusicId = (musicCurrent ? musicCurrent.id : ''));

    this.musicService
      .isPlayCurrentMusic$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isPlay = res);
  }

  public isLocale(music: Music){
    return music.type === TypeMusicSourceExternal.locale;
  }

  private changePlaylist(){
    this.playlist = this.musicPlaylistService.getPlaylistById(this.route.snapshot.params.idPlaylist);

    if(this.playlist){
      this.title = 'Playlist: ' + this.playlist.title;
      if(this.playlist.imagePath){
        const image = encodeURIComponent(this.playlist.imagePath)
          .replace(new RegExp('%5C', 'g'), '/')
          .replace('%3A', ':') ;

        this.image.nativeElement.style.backgroundImage = 'url(file://' + image + ')';
        this.imageContainer.nativeElement.style.borderColor = 'transparent';
        this.imageContainer.nativeElement.style.backgroundColor = 'transparent';
      }
      else{
        this.image.nativeElement.removeAttribute('style');
        this.imageContainer.nativeElement.removeAttribute('style');
      }
    }
  }

  ngOnDestroy() {
    if(this.destroyed){
      this.destroyed.next();
      this.destroyed.complete();
    }
  }

  togglePlay(music){
    this.selectedMusicId = music.id;

    //add to queue, other musics
    this.musicService.changeQueue(this.playlist.music);
    this.musicService.changeCurrentMusic(music);
  }

  toggleQueue(music: Music) {
    if(music.isExistInQueue){
      this.musicService.removeFromQueue(music);
    }
    else{
      this.musicService.addToQueue(music);
    }
  }

  deletePlaylist(){
    this.confirmService.open('Are you sure you want to delete the playlist "' + this.playlist.title + '"?', this._deletePlaylist);
  }

  _deletePlaylist(){
    this.musicPlaylistService.deletePlaylist(this.playlist.id);
    this.router.navigate(['/']);
  }

  remove(music: Music){
    this.musicPlaylistService.removeMusicFromPlaylist(this.playlist.id, music.id);
  }
}
