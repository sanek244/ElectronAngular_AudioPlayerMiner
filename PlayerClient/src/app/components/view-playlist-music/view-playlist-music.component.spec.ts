import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPlaylistMusicComponent } from './view-playlist-music.component';

describe('ViewPlaylistMusicComponent', () => {
  let component: ViewPlaylistMusicComponent;
  let fixture: ComponentFixture<ViewPlaylistMusicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPlaylistMusicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPlaylistMusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
