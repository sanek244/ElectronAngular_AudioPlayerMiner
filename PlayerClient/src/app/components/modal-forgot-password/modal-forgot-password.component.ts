import { Component, OnInit, OnDestroy } from '@angular/core';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {AuthorizationLocaleService} from '../../providers/authorization-locale.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal-forgot-password',
  templateUrl: './modal-forgot-password.component.html',
  styleUrls: ['./modal-forgot-password.component.scss']
})
export class ModalForgotPasswordComponent implements OnInit, OnDestroy {

  isOpen: boolean;
  isForgotWait: boolean;
  email: string;
  error: string;
  destroyed: Subject<any> = new Subject();

  constructor(private authorizationLocaleService: AuthorizationLocaleService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.authorizationLocaleService.isOpenModalForgotPassword$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => {
        this.isOpen = res;
        this.error = '';
        this.isForgotWait = false;
      });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  onClose() {
    this.authorizationLocaleService.closeModalForgotPassword();
  }

  sendNewPassword() {
    this.isForgotWait = true;
    this.authorizationLocaleService.resetPassword(this.email)
      .catch(error => {
        this.error = error;
        this.isForgotWait = false;
        throw '';
      })
      .then(() => {
        this.isForgotWait = false;
        this.toastr.success('A letter with a new password sent to you by email',
          'Success',
          {positionClass: 'toast-bottom-right', timeOut: 15000});
    });
  }
}
