import {Component, Input, Output, EventEmitter} from '@angular/core';
import {TypeMusicSourceInternal} from '../../enums/TypeMusicSource';

@Component({
  selector: 'app-music-list',
  templateUrl: './music-list.component.html',
  styleUrls: ['./music-list.component.scss']
})
export class MusicListComponent {

  @Input() items: string[] = [];
  @Input() titleItems: string[] = [];
  @Input() selectedItem: string = '';
  @Output() changeSelectedEvent: EventEmitter<string> = new EventEmitter();
  @Output() changeArrayEvent: EventEmitter<string[]> = new EventEmitter();

  constructor() {
  }

  clickItem(itemId: string) {
    this.changeSelectedEvent.emit(itemId);
  }

  onDrop(array){
    this.changeArrayEvent.emit(array);
  }
}
