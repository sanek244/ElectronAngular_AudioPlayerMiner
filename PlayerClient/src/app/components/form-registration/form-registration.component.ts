import { Component, OnInit } from '@angular/core';
import {AuthorizationLocaleService} from '../../providers/authorization-locale.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-form-registration',
  templateUrl: './form-registration.component.html',
  styleUrls: ['./form-registration.component.scss']
})
export class FormRegistrationComponent implements OnInit {

  error: string;
  email: string;
  password: string;
  passwordRepeat: string;
  name: string;
  birthday: Date;
  isAgreeTerms: boolean;
  isRegistrationWait: boolean;

  constructor(private authorizationLocaleService: AuthorizationLocaleService,
              private toastr: ToastrService) { }

  ngOnInit() {

  }

  registration() {
    this.error = '';

    if(!this.email) {
      this.error = 'Email address must not be empty!';
      return;
    }

    if(!this.password) {
      this.error = 'Password must not be empty!';
      return;
    }

    if(this.password !== this.passwordRepeat) {
      this.error = 'Passwords do not match!';
      return;
    }

    if(!this.name) {
      this.error = 'Name must not be empty!';
      return;
    }

    if(!this.isAgreeTerms) {
      this.error = 'You must read and agree to the terms and conditions to continue!';
      return;
    }

    this.isRegistrationWait = true;
    this.authorizationLocaleService.registration({
        email: this.email,
        password: this.password,
        confirmPassword: this.passwordRepeat,
      //TODO change server site for add name and birthday
      })
      .then(() => {
        this.isRegistrationWait = false;
        this.authorizationLocaleService.isSignIn = true;
        this.toastr.success('A message has been sent to your e-mail with activation of your account.',
          'Success registration',
          {positionClass: 'toast-bottom-right', timeOut: 15000});
      })
      .catch(error => {
        this.error = error;
        this.isRegistrationWait = false;
      });
  }
}
