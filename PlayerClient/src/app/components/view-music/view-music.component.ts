import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';
import {MusicAWSService} from '../../providers/music-aws.service';
import {MusicService} from '../../providers/music.service';
import {Music} from '../../models/Music';
import {TypeMusicSourceInternal} from '../../enums/TypeMusicSource';
import {MusicLocaleService} from '../../providers/music-locale.service';
import {Playlist} from '../../models/Playlist';
import {MusicPlaylistService} from '../../providers/music-playlist.service';
import {ToastrService} from 'ngx-toastr';
import {Helper} from '../../Helper';

@Component({
  selector: 'app-view-music',
  templateUrl: './view-music.component.html',
  styleUrls: ['./view-music.component.scss']
})
export class ViewMusicComponent implements OnInit, OnDestroy {


  //true: list of songs from local folders added by user.
  //false: Sons - list of all songs from purchased albums
  isLocale: boolean = false;

  music: Music[] = [];
  title: string;
  selectedMusicId : string = '';
  destroyed: Subject<any> = new Subject();
  playlists: Playlist[] = [];
  isPlay : boolean = false;

  sortedFieldByASC: string;

  public constructor(private route: ActivatedRoute,
                     private musicAWSService: MusicAWSService,
                     private musicLocaleService: MusicLocaleService,
                     private musicPlaylistService: MusicPlaylistService,
                     private musicService: MusicService,
                     private toastr: ToastrService)
  {
    this.isLocale = this.route.snapshot.data.isLocale || false;
  }

  ngOnInit() {
    if(this.isLocale){
      this.musicLocaleService
        .music$
        .pipe(takeUntil(this.destroyed))
        .subscribe(localeMusic => this.music = localeMusic);
    }
    else{
      this.musicAWSService
        .isChangeObserable
        .pipe(takeUntil(this.destroyed))
        .subscribe(() => {
          this.music = this.musicAWSService.Music;
        });
    }


    this.musicService
      .musicCurrent$
      .pipe(takeUntil(this.destroyed))
      .subscribe(musicCurrent => this.selectedMusicId = (musicCurrent ? musicCurrent.id : ''));

    this.musicPlaylistService
      .playlists$
      .pipe(takeUntil(this.destroyed))
      .subscribe(playlists => this.playlists = playlists);

    this.title = this.isLocale ? 'Locale Files' : 'All available Music';

    this.musicService
      .isPlayCurrentMusic$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isPlay = res);
  }

  ngOnDestroy() {
    if(this.destroyed){
      this.destroyed.next();
      this.destroyed.complete();
    }
  }

  togglePlay(music){
    this.selectedMusicId = music.id;

    //add to queue, other musics
    this.musicService.changeQueue(this.music);
    this.musicService.changeCurrentMusic(music);
  }

  toggleQueue(music: Music) {
    if(music.isExistInQueue){
      this.musicService.removeFromQueue(music);
    }
    else{
      this.musicService.addToQueue(music);
    }
  }

  playAll(){
    const music = this.music[0];
    this.selectedMusicId = music.id;

    //add to queue, other musics
    this.musicService.changeQueue(this.music);
    this.musicService.changeCurrentMusic(music);
  }

  createPlaylist(music: Music){
    this.musicPlaylistService.openModalCreate([music]);
  }

  addToPlaylist(playlist: Playlist, music: Music){
    this.musicPlaylistService.addMusicToPlaylist(playlist.id, music);
    this.toastr.success(Helper.format('Music "{0}" was successfully added to playlist "{1}"', music.metaData.titleFull, playlist.title),
      'Success',
      {positionClass: 'toast-bottom-right'});
  }

  orderBy(field : 'title' | 'album' | 'artist' | 'timePlay'){
    this.music = Helper.sortByField(this.music, (music : Music) => music.metaData[field], this.sortedFieldByASC !== field);
    this.sortedFieldByASC = this.sortedFieldByASC !== field
      ? field
      : '';
  }
}
