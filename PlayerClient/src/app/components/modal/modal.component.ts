import {Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() headerTitle: string;
  @Input() body: any;
  @Input() widthStyle: string;
  @Output() closeEvent: EventEmitter<void> = new EventEmitter();

  @ViewChild('modalContent') modalContent: ElementRef<HTMLDivElement>;

  constructor() { }

  ngOnInit() {
    if(this.modalContent && this.widthStyle){
      this.modalContent.nativeElement.style.width = this.widthStyle;
    }
  }

  close(){
    this.closeEvent.emit();
  }
}
