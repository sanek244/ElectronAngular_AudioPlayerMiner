import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';
import {Artist} from '../../models/Artist';
import {MusicAWSService} from '../../providers/music-aws.service';

@Component({
  selector: 'app-view-artists',
  templateUrl: './view-artists.component.html',
  styleUrls: ['./view-artists.component.scss']
})
export class ViewArtistsComponent implements OnInit, OnDestroy {

  destroyed: Subject<any> = new Subject();

  //true: list of all artists, whose albums user have purchased
  //false: all artists of the chosen genre
  isAllPurchased: boolean = false;

  //if isAllPurchased = false
  genre: string = '';

  artists: Artist[] = [];
  title: string;

  constructor(private musicAWSService: MusicAWSService,
              private route: ActivatedRoute)
  {
    this.isAllPurchased = this.route.snapshot.data.isAllPurchased || false;
    this.genre = this.route.snapshot.params.genre || '';
  }

  ngOnInit() {
    this.musicAWSService
      .isChangeObserable
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => {
        this.artists = this.isAllPurchased
          ? this.musicAWSService.Artists
          : this.musicAWSService.getArtistByGenre(this.genre);

        this.title = this.isAllPurchased ? 'All available artists' : this.genre;
      });
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }
}
