import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreatePlaylistComponent } from './modal-create-playlist.component';

describe('ModalCreatePlaylistComponent', () => {
  let component: ModalCreatePlaylistComponent;
  let fixture: ComponentFixture<ModalCreatePlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCreatePlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreatePlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
