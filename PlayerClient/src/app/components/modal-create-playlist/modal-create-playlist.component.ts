import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {MusicPlaylistService} from '../../providers/music-playlist.service';
import {ToastrService} from 'ngx-toastr';


const dialog = require('electron').remote.dialog;


@Component({
  selector: 'app-modal-create-playlist',
  templateUrl: './modal-create-playlist.component.html',
  styleUrls: ['./modal-create-playlist.component.scss']
})
export class ModalCreatePlaylistComponent implements OnInit, OnDestroy {

  isOpen: boolean;
  error: string;
  destroyed: Subject<any> = new Subject();

  imagePath: string;
  title: string;
  description: string;

  @ViewChild('image') image: ElementRef<HTMLDivElement>;

  constructor(private musicPlaylistService: MusicPlaylistService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.musicPlaylistService.isOpenModalCreate$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => {
        this.isOpen = res;
        this.error = '';
      });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  loadImage(){

    console.log('loadImage', this.image);
    this.error = '';
    const acceptableImageTypes = ['jpg','jpeg', 'png', 'gif'];

    dialog.showOpenDialog({
      properties: ['openFile'],
      filters: [
        {name: 'Images', extensions: acceptableImageTypes},
        {name: 'All Files', extensions: ['*']}
      ]},
      files => {
        if(!files){
          return;
        }

        if(acceptableImageTypes.indexOf(files[0].substr(-3)) > -1 || acceptableImageTypes.indexOf(files[0].substr(-4)) > -1){
          this.imagePath = files[0];

          const image = encodeURIComponent(this.imagePath)
            .replace(new RegExp('%5C', 'g'), '/')
            .replace('%3A', ':') ;

          this.image.nativeElement.style.backgroundImage = 'url(file://' + image + ')';
        }
        else{
          this.error = 'This file type is not supported!';
        }
      });
  }

  removeImage(event){
    this.imagePath = '';
    this.image.nativeElement.style.backgroundImage = '';
    event.preventDefault();
    event.stopPropagation();
  }

  onClose(){
    this.musicPlaylistService.closeModalCreate();
  }

  create(){
    this.error = '';

    if(!this.title){
      this.error = 'Playlist name must not be empty!';
      return;
    }

    this.musicPlaylistService.addPlaylist(this.title, this.description, this.imagePath);
    this.musicPlaylistService.closeModalCreate();
    this.toastr.success('Playlist successfully created!', 'Success', {positionClass: 'toast-bottom-right'});

    this.title = '';
    this.description = '';
    this.imagePath = '';
  }
}
