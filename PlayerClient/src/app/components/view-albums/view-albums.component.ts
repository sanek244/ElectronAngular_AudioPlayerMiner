import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';
import {MusicAWSService} from '../../providers/music-aws.service';
import {Album} from '../../models/Album';


@Component({
  selector: 'app-view-albums',
  templateUrl: './view-albums.component.html',
  styleUrls: ['./view-albums.component.scss']
})
export class ViewAlbumsComponent implements OnInit, OnDestroy {

  destroyed: Subject<any> = new Subject();

  //true: list of all purchased albums
  //false: all albums of the selected artist
  isAllPurchased: boolean = false;

  //if isAllPurchased = false
  idArtist: string;
  genre: string;

  albums: Album[] = [];
  title: string;

  public constructor(private route: ActivatedRoute,
                     private musicAWSService: MusicAWSService)
  {
    this.isAllPurchased = this.route.snapshot.data.isAllPurchased || false;
    this.idArtist = this.route.snapshot.params.idArtist || false;
    this.genre = this.route.snapshot.params.genre || false;
  }

  ngOnInit() {
    this.musicAWSService
      .isChangeObserable
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => {
        this.albums = this.isAllPurchased
          ? this.musicAWSService.Albums
          : this.musicAWSService.getAlbumsByArtistId(this.idArtist);

        if(this.isAllPurchased){
          this.title = 'All available albums';
        }
        else{
          const artist = this.musicAWSService.Artists.find(x => x.id === this.idArtist);
          this.title = !artist ? 'Albums' : (this.genre ? this.genre + ' - ' : '') + artist.name;
        }
      });
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }
}
