import {Component, ElementRef, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { MusicService } from '../../providers/music.service';
import { MusicMetaData } from '../../models/MusicMetaData';
import {Music} from '../../models/Music';
import {Logger} from '../../Logger';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {MusicLocaleService} from '../../providers/music-locale.service';
import {TypeMusicSourceExternal} from '../../enums/TypeMusicSource';
import {MusicAWSService} from '../../providers/music-aws.service';
import {AppConfig} from '../../../environments/environment';

@Component({
  selector: 'app-music-control-bar',
  templateUrl: './music-control-bar.component.html',
  styleUrls: ['./music-control-bar.component.scss']
})
export class MusicControlBarComponent implements OnInit, OnDestroy {

  @ViewChild('audioPlayer') audioPlayer: ElementRef<HTMLAudioElement>;
  @ViewChild('imgMusic') imgMusic: ElementRef<HTMLImageElement>;
  music: Music;
  destroyed: Subject<any> = new Subject();
  isPause: boolean = true;

  constructor(private musicService: MusicService,
              private musicLocaleService: MusicLocaleService,
              private musicAWSService: MusicAWSService) {
    this.changeCurrentMusic = this.changeCurrentMusic.bind(this);
  }

  ngOnInit() {
    this.musicService.musicCurrent$
      .pipe(takeUntil(this.destroyed))
      .subscribe(this.changeCurrentMusic);

    this.musicService.isPlayCurrentMusic$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => {
        if(res){
          if(this.isPause){
            this.audioPlayer.nativeElement.play();
          }
        }
        else{
          if(!this.isPause){
            this.audioPlayer.nativeElement.pause();
          }
        }
      });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  changeCurrentMusic(music:Music) {
    if(this.audioPlayer) {
      this.audioPlayer.nativeElement.pause();

      if(music) {
        this.music = music;
        this.audioPlayer.nativeElement.src = music.PathUri;
        this.audioPlayer.nativeElement.onloadedmetadata = () => {
          music.metaData.timePlaySecond = this.audioPlayer.nativeElement.duration;
        };
        const playPromise = this.audioPlayer.nativeElement.play();
        if (playPromise) {
          playPromise.catch(err => Logger.err(err.message));
        }

        if(music.metaData.pictureData) {
          if(music.type === TypeMusicSourceExternal.locale){
            this.imgMusic.nativeElement.src = 'data:image/png;base64,' + music.metaData.pictureData;
          }
          else{
            this.imgMusic.nativeElement.src = AppConfig.AWS_S3Host + AppConfig.AWS_bucketName + '/' + music.metaData.pictureData;
          }
        }
        else{
          this.imgMusic.nativeElement.src = '/assets/no-image.png';
        }

        //get meta data
        if(music.type === TypeMusicSourceExternal.locale && !music.metaData.album){
          this.musicLocaleService.getMetaData(music)
            .then(newMeta => {
              this.music.metaData = newMeta;
              if(newMeta.pictureData) {
                this.imgMusic.nativeElement.src = 'data:image/png;base64,' + newMeta.pictureData;
              }
            });
        }
      }
    }
  }

  toggleQueueMusicWindow() {
    this.musicService.isOpenQueueWindow = !this.musicService.isOpenQueueWindow;
  }

  togglePlayPause() {
    if(this.audioPlayer.nativeElement.paused){
      this.audioPlayer.nativeElement.play();
    }
    else{
      this.audioPlayer.nativeElement.pause();
    }
  }

  next(){
    this.musicService.nextMusic();
  }
  prev(){
    this.musicService.prevMusic();
  }
  toggleRandomPlay(){
    this.musicService.isShuffle = !this.isShuffle;
  }
  toggleRepeat(){
    this.musicService.isRepeat = !this.isRepeat;
  }

  get isRepeat(){
    return this.musicService.isRepeat;
  }
  get isShuffle(){
    return this.musicService.isShuffle;
  }

  onPause(){
    this.musicService.isPlayCurrentMusic = false;
    this.isPause = true;
  }
  onPlay(){
    this.musicService.isPlayCurrentMusic = true;
    this.isPause = false;
  }
}
