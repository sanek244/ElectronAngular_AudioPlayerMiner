import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicControlBarComponent } from './music-control-bar.component';

describe('MusicControlBarComponent', () => {
  let component: MusicControlBarComponent;
  let fixture: ComponentFixture<MusicControlBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicControlBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicControlBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
