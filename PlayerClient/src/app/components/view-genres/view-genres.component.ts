import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {MusicAWSService} from '../../providers/music-aws.service';

/**
 * On genres screen user sees tiles with all available music genres,
 * user can click on a tile and proceed to view all available artists for the selected genre
 */
@Component({
  selector: 'app-view-genres',
  templateUrl: './view-genres.component.html',
  styleUrls: ['./view-genres.component.scss']
})
export class ViewGenresComponent implements OnInit, OnDestroy {

  destroyed: Subject<any> = new Subject();

  items: string[] = [];

  constructor(private musicAWSService: MusicAWSService) {
  }

  ngOnInit() {
    this.musicAWSService
      .isChangeObserable
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => this.items = this.musicAWSService.Genres);
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }
}
