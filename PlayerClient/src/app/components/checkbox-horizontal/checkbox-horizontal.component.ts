import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox-horizontal',
  templateUrl: './checkbox-horizontal.component.html',
  styleUrls: ['./checkbox-horizontal.component.scss']
})
export class CheckboxHorizontalComponent implements OnInit {

  @Input() value: boolean;
  @Output() changeEvent: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  changeValue(){
    this.changeEvent.emit(!this.value);
  }
}
