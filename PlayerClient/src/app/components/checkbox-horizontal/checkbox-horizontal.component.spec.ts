import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxHorizontalComponent } from './checkbox-horizontal.component';

describe('CheckboxHorizontalComponent', () => {
  let component: CheckboxHorizontalComponent;
  let fixture: ComponentFixture<CheckboxHorizontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxHorizontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxHorizontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
