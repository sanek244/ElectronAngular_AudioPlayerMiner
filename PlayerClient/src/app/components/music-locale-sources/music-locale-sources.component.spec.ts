import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicLocaleSourcesComponent } from './music-locale-sources.component';

describe('MusicLocaleSourcesComponent', () => {
  let component: MusicLocaleSourcesComponent;
  let fixture: ComponentFixture<MusicLocaleSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicLocaleSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicLocaleSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
