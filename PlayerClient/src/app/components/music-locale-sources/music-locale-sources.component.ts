import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {MusicLocaleService} from '../../providers/music-locale.service';

@Component({
  selector: 'app-music-locale-sources',
  templateUrl: './music-locale-sources.component.html',
  styleUrls: ['./music-locale-sources.component.scss']
})
export class MusicLocaleSourcesComponent implements OnInit, OnDestroy {

  newSource: string;
  sources: string[];
  destroyed: Subject<any> = new Subject();

  constructor(private musicLocaleService: MusicLocaleService) {
  }

  ngOnInit() {
    this.musicLocaleService.sources$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.sources = res);
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }

  addSource(){
    if(this.musicLocaleService.getSources().indexOf(this.newSource) === -1){
      this.musicLocaleService.addSource(this.newSource);
    }
  }

  removeSource(source:string){
    this.musicLocaleService.removeSource(source);
  }
}
