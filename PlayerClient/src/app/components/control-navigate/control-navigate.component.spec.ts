import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlNavigateComponent } from './control-navigate.component';

describe('ControlNavigateComponent', () => {
  let component: ControlNavigateComponent;
  let fixture: ComponentFixture<ControlNavigateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlNavigateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlNavigateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
