import {Component, OnInit, OnDestroy} from '@angular/core';
import {RoutingService} from '../../providers/routing.service';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-control-navigate',
  templateUrl: './control-navigate.component.html',
  styleUrls: ['./control-navigate.component.scss']
})
export class ControlNavigateComponent implements OnInit, OnDestroy {

  destroyed: Subject<any> = new Subject();

  isCanPrev: boolean;
  isCanNext: boolean;

  constructor(public routingService: RoutingService) { }

  ngOnInit() {
    this.routingService.indexHistory$
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => {
        this.isCanPrev = this.routingService.isCanPrev;
        this.isCanNext = this.routingService.isCanNext;
      });
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  search(){

  }
}
