import {Component, OnInit, OnDestroy} from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import { User } from '../../models/User';
import { UserService } from '../../providers/user.service';
import { AuthorizationCommonService } from '../../providers/authorization-common.service';

@Component({
  selector: 'app-user-bar',
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.scss']
})
export class UserBarComponent implements OnInit, OnDestroy {

  user: User | null;

  destroyed: Subject<any> = new Subject();

  constructor(private userService: UserService,
              private authorizationCommonService: AuthorizationCommonService) {

    this.userService.user$
      .pipe(takeUntil(this.destroyed))
      .subscribe(user => this.user = user);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }

  logout(){
    this.authorizationCommonService.logout()
      .then(() => this.user = null);
  }

  openModal(){

  }
}
