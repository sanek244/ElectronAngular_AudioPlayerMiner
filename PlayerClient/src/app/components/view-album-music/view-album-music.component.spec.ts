import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAlbumMusicComponent } from './view-album-music.component';

describe('ViewAlbumMusicComponent', () => {
  let component: ViewAlbumMusicComponent;
  let fixture: ComponentFixture<ViewAlbumMusicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAlbumMusicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAlbumMusicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
