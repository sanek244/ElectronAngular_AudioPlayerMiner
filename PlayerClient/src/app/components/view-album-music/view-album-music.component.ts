import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';
import {Music} from '../../models/Music';
import {MusicAWSService} from '../../providers/music-aws.service';
import {MusicService} from '../../providers/music.service';
import {Artist} from '../../models/Artist';
import {MusicPlaylistService} from '../../providers/music-playlist.service';
import {ToastrService} from 'ngx-toastr';
import {Playlist} from '../../models/Playlist';
import {Helper} from '../../Helper';
import {AppConfig} from '../../../environments/environment';

/**
 * Song list of the selected album
 */
@Component({
  selector: 'app-view-album-music',
  templateUrl: './view-album-music.component.html',
  styleUrls: ['./view-album-music.component.scss']
})
export class ViewAlbumMusicComponent implements OnInit, OnDestroy {

  destroyed: Subject<any> = new Subject();

  genre: string;
  idArtist: string;
  idAlbum: string;

  music: Music[] = [];
  title: string;
  artist: Artist;

  selectedMusicId : string = '';
  playlists: Playlist[] = [];
  isPlay : boolean = false;

  @ViewChild('image') image: ElementRef<HTMLDivElement>;
  @ViewChild('imageContainer') imageContainer: ElementRef<HTMLDivElement>;

  public constructor(private route: ActivatedRoute,
                     private musicAWSService: MusicAWSService,
                     private musicPlaylistService: MusicPlaylistService,
                     private musicService: MusicService,
                     private toastr: ToastrService)
  {
    this.idAlbum = this.route.snapshot.params.idAlbum || false;
    this.idArtist = this.route.snapshot.params.idArtist || false;
    this.genre = this.route.snapshot.params.genre || false;
  }

  ngOnInit() {
    this.musicAWSService
      .isChangeObserable
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => {
        this.music = this.musicAWSService.getMusicByAlbumId(this.idAlbum);

        const album = this.musicAWSService.Albums.find(x => x.id === this.idAlbum);
        this.title = !album
          ? ''
          : (this.music.length ? this.music[0].metaData.genres[0] + ' - ' + this.music[0].metaData.artist + ' - ' : '') +
            album.title;

        if(album.picture){
          this.image.nativeElement.style.backgroundImage = 'url("' + AppConfig.AWS_S3Host + AppConfig.AWS_bucketName + '/' + album.picture + '")';
          this.imageContainer.nativeElement.style.borderColor = 'transparent';
          this.imageContainer.nativeElement.style.backgroundColor = 'transparent';
        }
        else{
          this.image.nativeElement.removeAttribute('style');
          this.imageContainer.nativeElement.removeAttribute('style');
        }
      });

    this.musicService
      .musicCurrent$
      .pipe(takeUntil(this.destroyed))
      .subscribe(musicCurrent => this.selectedMusicId = (musicCurrent ? musicCurrent.id : ''));

    this.musicPlaylistService
      .playlists$
      .pipe(takeUntil(this.destroyed))
      .subscribe(playlists => this.playlists = playlists);

    this.musicService
      .isPlayCurrentMusic$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isPlay = res);
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }

  togglePlay(music){
    this.selectedMusicId = music.id;

    //add to queue, other musics
    this.musicService.changeQueue(this.music);
    this.musicService.changeCurrentMusic(music);
  }

  toggleQueue(music: Music) {
    if(music.isExistInQueue){
      this.musicService.removeFromQueue(music);
    }
    else{
      this.musicService.addToQueue(music);
    }
  }

  playAll(){
    const music = this.music[0];
    this.selectedMusicId = music.id;

    //add to queue, other musics
    this.musicService.changeQueue(this.music);
    this.musicService.changeCurrentMusic(music);
  }

  createPlaylist(music: Music){
    this.musicPlaylistService.openModalCreate([music]);
  }

  addToPlaylist(playlist: Playlist, music: Music){
    this.musicPlaylistService.addMusicToPlaylist(playlist.id, music);
    this.toastr.success(Helper.format('Music "{0}" was successfully added to playlist "{1}"', music.metaData.titleFull, playlist.title),
      'Success',
      {positionClass: 'toast-bottom-right'});
  }
}
