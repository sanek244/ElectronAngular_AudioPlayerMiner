import {Component, OnInit, OnDestroy} from '@angular/core';
import { MinerService } from '../../providers/miner.service';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import { AuthorizationCommonService } from '../../providers/authorization-common.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-miner-checkbox',
  templateUrl: './miner-checkbox.component.html',
  styleUrls: ['./miner-checkbox.component.scss']
})
export class MinerCheckboxComponent implements OnInit, OnDestroy {

  isActiveApiServer: boolean = false;
  isOnMining: boolean = false;

  isProcessingMiner: boolean = false;
  destroyed: Subject<any> = new Subject();


  constructor(private minerService: MinerService,
              private authorizationCommonService : AuthorizationCommonService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.isProcessingMiner = true;

    this.minerService.isActiveServer$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isActiveApiServer = res);

    this.minerService.isOnMining$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isOnMining = res);

    this.minerService.isProcessingMiner$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isProcessingMiner = res);
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  toggle() {
    this.isProcessingMiner = true;
    if(this.isOnMining) {
      this.minerService.stopMiner();
    }
    else {
      if(!this.isActiveApiServer){
        this.toastr.error('Can not start the mining server.',
          'Error server',
          {positionClass: 'toast-bottom-right', timeOut: 5000});
        this.isProcessingMiner = false;
        return;
      }

      this.authorizationCommonService.refreshToken()
        .then(token => {
          this.minerService.startMiner(token)
            .catch(err => {
              this.toastr.error(err,
                'Error',
                {positionClass: 'toast-bottom-right', timeOut: 15000});
            });
        });

    }
  }
}
