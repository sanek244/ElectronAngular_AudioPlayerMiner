import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinerCheckboxComponent } from './miner-checkbox.component';

describe('MinerCheckboxComponent', () => {
  let component: MinerCheckboxComponent;
  let fixture: ComponentFixture<MinerCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinerCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinerCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
