import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationSocialNetworksComponent } from './authorization-social-networks.component';

describe('AuthorizationSocialNetworksComponent', () => {
  let component: AuthorizationSocialNetworksComponent;
  let fixture: ComponentFixture<AuthorizationSocialNetworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationSocialNetworksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationSocialNetworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
