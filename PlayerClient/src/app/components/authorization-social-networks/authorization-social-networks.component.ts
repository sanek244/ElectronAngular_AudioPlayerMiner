import { Component, OnInit } from '@angular/core';
import {Logger} from '../../Logger';
import {AuthorizationTwitterService} from '../../providers/authorization-twitter.service';
import {AuthorizationFacebookService} from '../../providers/authorization-facebook.service';
import {AuthorizationGoogleService} from '../../providers/authorization-google.service';

@Component({
  selector: 'app-authorization-social-networks',
  templateUrl: './authorization-social-networks.component.html',
  styleUrls: ['./authorization-social-networks.component.scss']
})
export class AuthorizationSocialNetworksComponent implements OnInit {

  error = '';

  constructor(public AuthorizationGoogleService: AuthorizationGoogleService,
              private AuthorizationTwitterService: AuthorizationTwitterService,
              private AuthorizationFacebookService: AuthorizationFacebookService) {
  }

  ngOnInit() {
  }

  public loginGoogle(): void {
    this.error = '';
    this.AuthorizationGoogleService
      .login()
      .catch(err => {
        if(typeof err === 'object' && 'type' in err) {
          if(err.type === 'tokenFailed') {
            Logger.log('Google loading... Please, repeat authorization.');
            this.error = 'Google loading... Please, repeat authorization.';
            this.loginGoogle();
            return;
          }
        }

        this.error = err;
      });
  }


  public loginFacebook(): void {
    this.error = '';
    this.AuthorizationFacebookService.login()
      .catch(err => this.error = err);
  }

  public loginTwitter(): void {
    this.error = '';
    this.AuthorizationTwitterService.login()
      .catch(err => this.error = err);
  }
}
