import { Component, OnInit, OnDestroy } from '@angular/core';
import {ConfirmService} from '../../providers/confirm.service';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit, OnDestroy {

  isOpen: boolean;
  text: string;
  destroyed: Subject<any> = new Subject();

  constructor(private confirmService: ConfirmService) { }

  ngOnInit() {
    this.confirmService.isOpen$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.isOpen = res);

    this.confirmService.text$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.text = res);
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  onClose() {
    this.confirmService.close();
  }

  ok(){
    this.confirmService.callbackOk();
    this.confirmService.close();
  }
}
