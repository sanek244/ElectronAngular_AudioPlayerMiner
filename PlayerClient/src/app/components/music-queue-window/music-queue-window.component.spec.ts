import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicQueueWindowComponent } from './music-queue-window.component';

describe('MusicQueueWindowComponent', () => {
  let component: MusicQueueWindowComponent;
  let fixture: ComponentFixture<MusicQueueWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicQueueWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicQueueWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
