import { Component, OnInit, OnDestroy } from '@angular/core';
import {MusicService} from '../../providers/music.service';
import {Music} from '../../models/Music';
import {takeUntil} from 'rxjs/internal/operators';
import {Subject} from 'rxjs/index';
import {TypeMusicSourceInternal} from '../../enums/TypeMusicSource';

@Component({
  selector: 'app-music-queue-window',
  templateUrl: './music-queue-window.component.html',
  styleUrls: ['./music-queue-window.component.scss']
})
export class MusicQueueWindowComponent implements OnInit, OnDestroy {

  public isOpen: boolean = false;
  public music: any = {};
  public musicIdSelected: string = '';
  destroyed: Subject<any> = new Subject();

  constructor(private musicService: MusicService) {
  }

  ngOnInit() {
    this.musicService.isOpenQueueWindow$
      .pipe(takeUntil(this.destroyed))
      .subscribe(isOpen => this.isOpen = isOpen);

    this.musicService.musicQueue$
      .pipe(takeUntil(this.destroyed))
      .subscribe(music => {
        this.music = {};
        music.map(x => this.music[x.id] = x);
      });

    this.musicService.musicCurrent$
      .pipe(takeUntil(this.destroyed))
      .subscribe(musicCurrent => this.musicIdSelected = (musicCurrent ? musicCurrent.id : ''));
  }

  ngOnDestroy(){
    this.destroyed.next();
    this.destroyed.complete();
  }

  get MusicIds(){
    return Object.keys(this.music);
  }
  get MusicTitles(){
    return Object.values(this.music).map((x: Music) => x.metaData.titleFull);
  }

  closeWindow(){
    this.musicService.isOpenQueueWindow = false;
  }

  onChangeSelectedMusic(musicId: string){
    this.musicService.changeCurrentMusic(this.music[musicId]);
  }

  onChangeQueue(musicIds: string[]){
    const newArray = musicIds.map(id => this.music[id]);
    this.musicService.changeQueue(newArray, false);
  }

  clear(){
    this.musicService.changeQueue([]);
  }
}
