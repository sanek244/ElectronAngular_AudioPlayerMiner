import { Component, OnInit, OnDestroy } from '@angular/core';
import {Subject} from 'rxjs/index';
import {takeUntil} from 'rxjs/internal/operators';
import {Playlist} from '../../models/Playlist';
import {MusicPlaylistService} from '../../providers/music-playlist.service';

@Component({
  selector: 'app-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss']
})
export class MenuLeftComponent implements OnInit, OnDestroy {

  playlists: Playlist[];
  destroyed: Subject<any> = new Subject();

  constructor(public musicPlaylistService: MusicPlaylistService) {

  }

  ngOnInit() {
    this.musicPlaylistService.playlists$
      .pipe(takeUntil(this.destroyed))
      .subscribe(res => this.playlists = res);
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  onToggleMiner(){

  }
}
