export enum TypeMusicSourceInternal{
  locale = 'locale',
  AWS = 'AWS',
  queue = 'queue',
  playlist = 'playlist'
}
export enum TypeMusicSourceExternal{
  locale = 'locale',
  AWS = 'AWS',
}
