import { app, BrowserWindow, screen } from 'electron';

const { exec } = require('child_process');
const os = require('os');

let win;
const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const dev = true;//args.some(val => val === '--dev');

// Instantiate Express App
const express = require('express');
const appExpress = express();
let serverExpress = null;

function createWindow() {

  const size = screen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height,
    title: 'Miner Audio Player',
    webPreferences : {
      webSecurity :  false //for HTML5 Audio load locale files
    }
  });

  const port = 4200;
  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.webContents.openDevTools();
  }
  else{

    // Angular
    appExpress.use('/', express.static(__dirname + "/dist"));

    serverExpress = appExpress.listen(port);

    if(dev){
      win.webContents.openDevTools();
    }
  }


  win.loadURL('http://localhost:' + port);

  win.focus();

  // Emitted when the window is closed.
  win.on('closed', () => {
    close();
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  win.on('close', () => {
    close();
  });
}

function close(){
  if(os.type() === 'Windows_NT'){
    exec('taskkill /f /t /im bfgminer.exe');
  }
  else{
    exec('killall -9 bfgminer');
  }
}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', createWindow);

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    if(serverExpress !== null){
      serverExpress.close();
    }
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}
